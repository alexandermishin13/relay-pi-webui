# relay-pi-webui #

[![WebUI for relays](/relay-pi-webui.png?raw=true "Web management for GPIO-controlled relays")](https://demo-aquarium.mh.net.ru/)

## About ##

Web user interface for visualizing the control of level N relays. Control
can be local, via gpio, or remote, for relays based on **tasmota** firmware.

I wrote them for my aquarium project. Relays is used to power up a number
of things, such as LED-lights and air pumps. This relays module is contolled
on ARM PC (Orange PI Zero, 512M) via GPIO bus using utilities from my other 
project **relay-pi**. This one is intended to visualize the switchings made by
the relay.

## Requirements ##

For run the **relay-pi-webui** You need:

* Manageable relay for DIY with one or more channels;

![DC 5V Relay Module](/DC-5V-Relay-Module.jpg?raw=true "DC 5V Relay Module")
* ARM PC with GPIO bus powered by OS Armbian (Raspbian) Linux or FreeBSD (I prefer last one);

![Orange Pi Zero](/Orange-Pi-Zero.jpg?raw=true "OrangePi Zero")
* Web server with PHP backend (I prefer Nginx with php-fpm).

## Downloads ##

You can download the project with its submodule **relay-pi** by:
```shell
git clone --recursive git@gitlab.com:alexandermishin13/relay-pi-webui.git
```

## Installation ##

* Just copy directories `html/` (and maybe `bin/` and `db/` if You haven't done it before) to Your project folder.
I shall suggest You either `/opt/<project>/` or `/srv/<project>/` places.
For example:
```
    /srv/<project>/http/
    ./bin/[relay-pi]
    ./db/
    ./html/
    ./html/images/
    ...
```
* You need PHP modules:
  * `php-json` - config file;
  * `php-session` - for authenthication;
  * `php-sockets` - for **ntp**.
* You may need also Python modules for **relay-pi**:
  * `py-python-crontab`;
  * `py-json` (some recommends `py-simplejson`, it also works);
  * `py-wand` - for manual redrawing schedule charts (obsolete).

* Change owner of `./db/` directory to an user from which web service running (usually, `www` or
`www-data`).

## First run ##

I recommend You next order of running the utilities:

* Running **relay_init.py** makes an example of config file `relay.json`.
I recommend to place it to `../db/` folder, espesially if You plan a web GUI to Your relay.
This is the best place for this file, but you can change it if you like.
* Edit the `relay.json`. You need to check a `system` section:
  * `username` is a system user a web-server running as. Also it is a user for cron jobs running.
  * `auth` is a `user:password` pairs list for a web authentication. That's why we place the `config.json`
  to outside a web root directory `./html/`. You may want to do something like
  `mkpasswd -m sha-256 <your_very_secret_password>`
  (or `openssl passwd -5 <your_very_secret_password>`)
  and use its hash instead of a clear text password in the auth-pair.
  You can also to edit an `outlets` section for set GPIO-pins and timers of physical relays right.
* Then run **relay_cron.py**. It will create and apply a crontab file for `system->username` user with timers
changed by You.

## Modules

When I realized that my system was finally working well, it had only buttons
and schedule bars. But I wanted much more...

I thought that things the system can do are very common - I personally want
to use my system to automate and control my aquariums, and someone may want
to light and water their plants. This can be done using modules. I started
writing them from my favorite subject, aquariums.

### Module:aqua ###

This web module can be enabled by setting the `$module_aqua` variable of the
`config.inc.php` file to true.

This module periodically renders a controlled object, taking into account
the changes made by relay switching.

This job uses an html table with a class defined in `config.inc.php` to display
the ordered pieces of an entire image of the monitored object (or multiple
tables with unique classes for multiple monitored ones). Each definition of a
relay of this class contains an array of two variants of the piece of the entire
image corresponding to this relay - one for its on and off state. When the
object is periodically redrawn, the entire image is assembled from all parts,
each of which reflects the state of its relay.

For localized description You can define a constant in `/languages/<lang>.inc.php`
like an 'OUTLET1_DESC' for node name 'outlet1'. Same for a class node as a
description for whole object image. If no constant defined the name itself
will be displayed on its place.

The folder `./images/<module>/collection` is not used for visual effects and
can be safely deleted or moved. It's just a collection of pictures suitable
for the module.

### Module:aqua-z ###

This web module is a successor of the ```aqua``` module, I think. It is in
development now. ```z``` here stands for a web term ```z-index```, which I
planned as a basis of visualization, but has not yet reached this stage of
development. The module uses `css` based animation for fish - nothing
extraordinary, but with it the aquarium looks much more alive.

The folder `./images/<module>/collection` is not used for visual effects and
can be safely deleted or moved. It's just a collection of pictures suitable
for the module. I personally leave it in place and just hardlink from the
collection to `./images/aqua-z/` with the proper names.

### Module:ntp_status ###

This web module may be turned on by set `config.inc.php` variable `$module_ntp_status` to true.

If You have got local NTP server (and You almost certainly have), You can
monitor it by a message right under a title.
Right now I think that a stratum and reference is enough. (They change their colors individually.
Actually their classes by javascript, like a 'good', 'warn', 'alarm'.)
You can configure it by `/styles/aqua.css`. Stratum is good for less than 3, warn for equal 3,
alarm for larger than 3. Reference is good for host defined as local (You can write anything
but 'public pool') in `ntp_status.php`
otherwise it is `warn` and displayed as 'public pool' (for security reason).

You of course can monitor a remote NTP after change ip-address `127.0.0.1`
by its one in `ntp_status.php` but I cannot see why.

### Module:temperature ###

This web module may be turned on by set `config.inc.php` variable `$module_temperature` to true.

For configuration of sensors You can edit a $thermometers associative array, which is following an
$images one structure for classes (as monitored objects). For each one Yow can define a thermometer
(I use two **ds18b20** sensors connected by `1-wire` bus with 10kOm as pin up to **Vcc**). A `romid`
parameter is mandatory and must contain a sensor ID specified in a notation according to Your OS
('28:07:45:94:97:05:03:00' for FreeBSD, '28-07459497050300' for Linux). Methods for obtaining a sensor value
for both OSs are different.

#### Freebsd FDT-overlay installation ####

For FreeBSD You may need add to **/boot/loader.conf**:
```ini
ow_load="YES"
ow_temp_load="YES"
owc_load="YES"
fdt_overlays="`your_other_overlays`,sun8i-h3-w1-gpio"
```

Example of source `sun8i-h3-w1-gpio.dtso` for pin `PA13` of my OrangePi PC:

```
/dts-v1/;
/plugin/;

/ {
    compatible = "allwinner,sun8i-h3";
};

&pio {
    w1_pins: w1_pins {
        allwinner,pins = "PA13";
        allwinner,function = "gpio_in";
#       allwinner,drive = <0>;
#       allwinner,pull = <1>; // on
    };
};

&{/} {
    onewire@0 {
        compatible = "w1-gpio";
        pinctrl-names = "default";
        pinctrl-0 = <&w1_pins>;
        gpios = <&pio 0 13 0>; /* PA13 */
        status = "okay";
    };
};
```

You need to compile it to a blob and place the blob to **/boot/dtb/overlays/**:

```shell
/usr/bin/dtc -@ -I dts -O dtb -o ./sun8i-h3-w1-gpio.dtbo ./sun8i-h3-w1-gpio.dtso
```

#### Linux FDT-overlay installation ####

For Linux (Armbian or Raspbian), Google has
[plenty of examples](https://www.google.com/search?q=armbian+onewire).

## Remote control ##

If You find that a web interface is not as good as a remote control, a
following kmod driver and a daemon can be useful for You:
[rcrecv-kmod](https://gitlab.com/alexandermishin13/gpiorcrecv-kmod) and
[rcrecv-daemon](https://gitlab.com/alexandermishin13/rcreceiver).

The kernel driver reads the code from the gpio RF module, sent by the RF
remote control (for example, 413 MHz), and the daemon switches the gpio pin
for the given code as you defined it. Both of them can turn a lighting
or an air compressor on or off by rc signal.

They are not part of the project, but they can make management more convenient.
I use it with the remote control fixed on the near side of an aquarium stand.

## Thanks

* [Nirmani Warakaulla](https://nirmaniwarakaulla.medium.com/)
for her [3 Step Guide to Create a Fish Tank Only Using HTML and CSS](https://medium.com/nerd-for-tech/how-to-create-a-fish-tank-with-html-css-c98aca39ae85);
