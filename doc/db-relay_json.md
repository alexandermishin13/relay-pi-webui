# relay-pi-webui

## db/relay.json

### About

A sort of main database with the system and devices' common options.
Its format is json as it is easiest to load/store from an array in the case
of ```PHP``` or a dictionary in the case of ```python```. A good place for it
is outside the html root, as that's where the passwords are stored.

### system

This configuration section is simple. It contains only the following
three parameters:
* ***username*** is the system user under which the http-server is running
  ("www-data" in ```Linux```). This user must have permissions to switch
   ```gpio``` pins, and this is the user who will own the crontab jobs to
   switch these pins on schedule;
* ***plaintext*** is the option to allow/deny the plaintext paswords.
  By default, the ***plaintext*** sets to ```true``` what means that the
  plain text passwords is allowed. It's good to start with something like
  ```guest```:```guest``` for username and password. But later, it is better
  to replace the password with its hash and set the ***plaintext*** to
  ```false```;
* ***auth*** is a list of username and password pairs, such as:
  * ```guest```:```guest```
  * ```admin```:```$2y$10$ouC8.8RfzeW2CixUHujUM.2/P4.u3c/QYoBXWmA7nxvMshByhAm62```

<details>
<summary>Example of system section</summary>

```json
"system": {
  "username": "www-data",
  "plaintext": true,
  "auth": {
    "admin": "adminpasswd",
    "chief": "chiefpasswd"
  }
}
```
</details>

### platforms

The ***platforms*** section contains descriptions of groups of elementary
devices, often assembled on the same electronic board and controlled in the
same way. Specifically, this is a list of named units characterized by platform
type. For example, the platform ***local*** has a ***type*** ```gpio```.
This platform is not recomended to be modified as its values are used as a
fallback in the code. Moreover, I don't see the point in creating another
```gpio```-type platform, unless you like the very name ***local***.

At the moment there are only two types of platforms: ```gpio``` and
```tasmota```. The platform's type is ```gpio``` if you didn't set any.

If you set ***type*** to ```tasmota``` you also need to set the ***host***
parameter. If you haven't done this, the "host" value is assumed to be
"192.168.14.1".

You can name your platforms whatever you like.

<details>
<summary>Example of platforms section</summary>

```json
"platforms": {
  "local": {
    "type": "gpio"
  }
  "nodemcu": {
    "type": "tasmota",
    "host": "192.168.1.10"
  }
}
```
</details>

### outlets

The ***outlets*** section is used to configure the relays and this
is the most complex section. In addition to setting the relay control
parameters, each of the relays can contain a schedule for automatically
turning it on or off. The options for the ***outlets*** section are as
follows:
* ***active*** - despite the fact that the relay is configured, you can
  enable it to be used by the software, or you can disable it;
* ***platform*** - here you specify one of the configured platforms
  from the ***platforms*** section. Use the platform name, not the
  platform type name;
* ***pin*** is number of the pin or the channel for this relay in terms
  of configured platform;
* ***timers*** is a list of actions on time. Each action has its own
  options, such as:
  * ***action*** - "on" or "off" for the relay;
  * ***time*** is the time for the action as string "%H:%M";
  * ***weekday*** is a comma separated list of days of the week where
  1 is Monday and 7 is Sunday (I also consider 0 as Sunday but this
  is not tested). If not set, it means every day of the week.

<details>
<summary>Example of outlet section</summary>

```json
"outlets": {
  "outlet1": {
    "active": true,
    "platform": "local",
    "pin": 12,
    "timers": [
      {
        "action": "on",
        "time": "09:00"
      },
      {
        "action": "off",
        "time": "12:30"
      }
      {
        "action": "on",
        "time": "17:00"
      },
      {
        "action": "off",
        "time": "22:00"
      }
    ]
  },
  "outlet2": {
    "active": true,
    "platform": "nodemcu",
    "pin": 1,
    "timers": [
      {
        "action": "on",
        "time": "10:00",
        "weekday": [1,2,3,4,5]
      },
      {
        "action": "off",
        "time": "15:00",
        "weekday": [1,2,3,4,5]
      }
    ]
  }
}
```
</details>

### thermometers

The ***thermometers*** section is used to configure the temperature sensors.
You can use ```ds1820``` sensors connected to the ```OneWire``` bus of your
SOC with platform type ```local``` or of any other device based on
```tasmota``` firmware with platform type of the same name.
* ***platform*** - here you specify one of the configured platforms
  from the ***platforms*** section. Use the platform name, not the platform
  type name;
* ***romid*** (mandatory) This is a hexadecimal string representation of
  the 8-byte sensor hardware address. The ```romid``` representation format
  may vary slightly depending on the operating system or software of the
  device to which the sensor is connected;
* ***correction*** You could correct slightly a sensor value if You want.

<details>
<summary>Example of thermometers section</summary>

```json
"thermometers": {
  "therm1": {
    "platform": "local",
    "romid": "28:00:00:00:00:00:00:00",
    "correction": 0.8
  },
  "therm2": {
    "platform": "nodemcu",
    "romid": "030597944507",
    "correction": 0.5
  }
}
```
</details>
