# relay-pi-webui

## Temperature

### About

A "temperature" module uses for read an object temperature from sensors and
display them on the object image (I did the project primarily for my
aquariums, so this is the aquarium whose image will be used in my case).
Of course, you are free to draw your own picture that matches your object.

Only `ds1820` sensors supported by now.

The sensors can be local or remote and can be declared in `db/relay.json`
file section ***thermometers*** with its unique name and several parameters.

An unique name is an `id` parameter of `html`-element tag which will be
used for display the sensor value.

List of parameters for json config you can find
[here](db-relay_json.md#thermometers "Thermometer's section of relay.json").

Another config file for thermometers monitored by this software is
___/config/config_temperature.inc.php___.
The file contains named sensors' sections bound with their objects by
object's container id. If you don't have any text container with the
thermometer's id on your webpage a span-container with thermometer's id will
be created inside the container with the object's id on runtime.

<details>
<summary>Example of config_thermometers.inc.php</summary>

```php
$thermometers = [
  'therm1' => [
    'id' => 'aqua1',
    'decimals' => 1
  ],
];
```
</details>

### Local sensors

Local sensors are sensors connected to an `OneWire` bus of the platform on
which installed that software.

You may need to install FDT-overlay for support the `OneWire` bus
and load kernel modules for the bus and temperature sensores before
the OS could see the sensors. See information for Your platform and OS on
Internet (You can also find some useful info in `README.md` section
`Module:temperature`).

### Remote sensors

Remote sensors connect to a remote device running on ```tasmota``` firmware
and their values can be read by http[s] protocol.

You need to connect Your sensor to ```3V```, ```G```, ```Data#``` pins by
```red```, ```black```, ```yellow``` wires correspondingly. You may also need
a ~4.7K resistor to pull the data pin to power.
See Internet for details about connection ```OneWire``` sensors to your device..
