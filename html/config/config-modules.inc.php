<?php

/* List of modules:
    'ntp-status': Shows an ntp stratum and a time source for 'ntpd'
    'aqua': A table based visualisation of objects (A simplest one, but may be useful for someone)
    'aqua-z': A stack based visualisation of objects
    'sensors': Shows a sensors map (not finished yet)
    'temperature': Shows a temperature of anobject on the objects image (See modules 'aqua' an 'aqua-z')
    'plants': It could be your module
 */
$modules = [
    'ntp-status' => [
	'active' => true,
	'code' => 'ntp_status.inc.php',
	'style' => ['ntp_status.css']
    ],
    'test' => [
	'active' => false,
	'code' => 'test.inc.php'
    ],
    'aqua' => [
	'active' => false,
	'code' => 'aqua.inc.php',
	'style' => ['aqua.css']
    ],
    'aqua-z' => [
	'active' => true,
	'code' => 'aqua-z.inc.php',
	'style' => ['styles/styles.css', 'styles/fish.css', 'styles/animation.css', 'styles/tank_aqua1.css', 'styles/tank_aqua2.css']
    ],
    'sensors' => [
	'active' => true,
	'code' => 'sensors.inc.php',
	'style' => ['sensors.css']
    ],
    'temperature' => [
	'active' => true,
	'code' => 'temperature.inc.php',
	'style' => ['temperature.css']
    ],
    'plants' => [
	'active' => false,
	'code' => 'plants.inc.php',
	'style' => ['plants.css']
    ]
];

?>
