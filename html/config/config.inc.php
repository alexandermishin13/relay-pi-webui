<?php
ini_set('session.cookie_secure', 1);
ini_set('session.cookie_samesite', 'Strict');

$config = [];

/* relay-pi absolutebsolute path to relay-pi folders */
$config['document_root'] = $_SERVER['DOCUMENT_ROOT'];
#$config['document_root'] = '/srv/http/mouse.home/html';
$config['path_root'] = realpath($config['document_root'].'/..');

/* relay-pi file names */
$config['cmd_init'] = 'bin/relay_init.py';
$config['cmd_switch'] = 'bin/relay_set.py';

/* Absolute path to config file */
$config['db_json'] = 'db/relay.json';

/* Time zone string or 'auto' */
$config['timezone'] = 'Europe/Moscow';

/* Run in demo mode (Nothing really switches, only visualization).
   If you ready to control your relays and sensors change it to 'false'
   And, of course, You can use here something like:
    if ($SERVER['HTTP_HOST'] === 'www.example.com') {
        $config['demo_mode'] = ...
 */
$config['demo_mode'] = true;

$config['title'] = 'Aquarium';

$config['meta_content'] = '';

$config['meta_viewport'] = <<<META_VIEWPORT
width=device-width, initial-scale=1, maximum-scale=1.5, user-scalable=yes
META_VIEWPORT;

$config['syslog'] = true;

/* Appearance of relay switches: 'round', 'sphere' or empty (square) */
$config['switch_style'] = 'round';

/* Weekday colors (0:Mon ... 6:Sun) */
$chart_color = [
    'lightskyblue', 'lightskyblue', 'lightskyblue', 'lightskyblue', 'lightskyblue',
    'lightsalmon', 'lightsalmon'
];
/* Image format: 'canvas' or 'svg' (default) */
$chart_format = 'svg';

/* 1x1 pixel transparent image */
$image_blank = '/images/blank.png';

/* Get name from name by search a constant like a NAME_DESC
 * If no such constant defined name itself will be used.
 */
function get_name($name) {
    $n = strtoupper($name).'_DESC';
    return (defined($n) ? constant($n) : $name);
}

/* Try to load a config json
 */
function get_config_json($config_json, $json_respond=false) {
    $json = [];
    if (file_exists($config_json)) {
        $data = file_get_contents($config_json);
        $json = json_decode($data, true);
        if($json === null) {
            $err_msg = sprintf(ERROR_CANNOT_DECODE_JSON, basename($config_json));

            if ($json_respond)
                echo "{'error':'invalidConfigFile','message':'$err_msg'}";
            else
                echo $err_msg;

                exit(2);
        }
    }
    else {
        $err_msg = sprintf(ERROR_CANNOT_OPEN_FILE, basename($config_json));

        if ($json_respond)
            echo "{'error':'noConfigFile','message':'$err_msg'}";
        else
            echo $err_msg;

            exit(1);
    }

    return $json;
}

/* Set default value to a platform name
 */
function platform_name($name) {
    if (!$name)
        $name = 'local';

    return $name;
}

/**
 * Autoload classes
 */
spl_autoload_register(function ($class) {
    global $config;

    $file = $config['document_root'] . '/includes/' . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
    if (file_exists($file)) {
        require_once $file;
        return true;
    }
    return false;
});
?>
