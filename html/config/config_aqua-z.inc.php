<?php
/* Define images parts as an associative array of named relay outlets grouped by table classes.
   They names are the same as ones from json-file, where its values are associative arrays of
   image parts parameters.
   A list 'image' contains relay states images.
   parameter is used by 'javascript'.
 */
$aqua_z_images = [
    /* Class of table for a whole object image.
     * You can configure multiple classes for multuple controlled objects.
     */
    'aqua1' => [
        'decorations' => [
            /* Stack of images 
                'id' => [
                    'class' => class,
                    'width': width parameter of img,
                    'height': height parameter of img,
                    'image' => url,
                    ...
                ]
             */
            'fishtank1' => [
                'class' => 'tank',
                'image' => '/plugins/aqua-z/images/fishtank1.svg'
            ],
            'ground1' => [
                'class' => 'ground',
                'image' => '/plugins/aqua-z/images/ground1.png'
            ],
            'mainLighting' => [
                'class' => 'lighting',
                'image' => '/plugins/aqua-z/images/mainLighting.svg'
            ],
            'airpump' => [
                'class' => 'airpump',
                'left' => '260px',
                'image' => '/plugins/aqua-z/images/airpump.svg'
            ],
            'reflection1' => [
                'class' => 'reflection',
                'image' => '/plugins/aqua-z/images/reflection.svg'
            ],
            'echinodorus5' => [
                'class' => 'plants',
                'image' => '/plugins/aqua-z/images/echinodorusAmazonicus.png'
            ],
            'echinodorus7' => [
                'class' => 'plants',
                'image' => '/plugins/aqua-z/images/echinodorusBleheri.png'
            ],
            'echinodorus6' => [
                'class' => 'plants',
                'image' => '/plugins/aqua-z/images/echinodorusAmazonicus.png'
            ],
            'alternanthera1' => [
                'class' => 'plants',
                'image' => '/plugins/aqua-z/images/alternantheraLilacina.png'
            ],
        ],
        'fish' => [
            'gourami' => [
                'z-index-base' => 10,
                'surface' => true,
                'image' => [
                    '/plugins/aqua-z/images/fish1.png',
                    '/plugins/aqua-z/images/fish2.png',
                ]
            ],
        ],
        'switches' => [
            /* Set of img-elements figures the switch
                'class': class for cascading styles,
                'width': width parameter of img,
                'height': height parameter of img,
                'images': overlays indexed by switch status number
                          (in my case 0 stands for 'on', 1 stands for 'off')
             */
            'outlet1' => [
                'class' => 'light',
                'width' => '380px',
                'image' => [
                    'ON' => '/plugins/aqua-z/images/light1.png',
                    'OFF' => $image_blank
                ]
            ],
            'outlet3' => [
                'class' => 'air',
                'width' => '120px',
                'image' => [
                    'ON' => '/plugins/aqua-z/images/air1.png',
                    'OFF' => $image_blank
                ]
            ],
        ]
    ],
    'aqua2' => [
        'decorations' => [
            'fishtank2' => [
                'class' => 'tank',
                'image' => '/plugins/aqua-z/images/fishtank2.svg',
            ],
            'ground2' => [
                'class' => 'ground',
                'image' => '/plugins/aqua-z/images/ground2.png'
            ],
            'sideLighting' => [
                'class' => 'lighting',
                'image' => '/plugins/aqua-z/images/sideLighting.svg'
            ],
            'reflection2' => [
                'class' => 'reflection',
                'image' => '/plugins/aqua-z/images/reflection.svg'
            ],
            'cryptocoryne4' => [
                'class' => 'plants',
                'image' => '/plugins/aqua-z/images/cryptocoryneBarclaya_small.png',
            ],
            'cryptocoryne3' => [
                'class' => 'plants',
                'image' => '/plugins/aqua-z/images/cryptocoryneBalansae.png',
            ],
            'cryptocoryne2' => [
                'class' => 'plants',
                'image' => '/plugins/aqua-z/images/cryptocoryneBarclaya.png',
            ],
        ],
        'fish' => [
            'school' => [
                'z-index-base' => 20,
                'image' => [
                    '/plugins/aqua-z/images/fish/redNeon1.png',
                    '/plugins/aqua-z/images/fish/redNeon2.png',
                    '/plugins/aqua-z/images/fish/redNeon3.png',
                    '/plugins/aqua-z/images/fish/redNeon4.png',
                ]
            ],
        ],
        'switches' => [
            'outlet4' => [
                'class' => 'light',
                'width' => '260px',
                'image' => [
                    'ON' => '/plugins/aqua-z/images/light2.png',
                    'OFF' => $image_blank
                ]
            ],
        ]
    ],
];

/* Enrich the images' configuration with decorations' dynamic addribites
 */
function mergeDecorations(&$array, $image_attributes) {
    global $config;

    foreach($array as $tank => $objects) {
        if (
            !isset($objects['decorations']) ||
            !isset($image_attributes[$tank]['decorations'])
        ) {
            continue;
        }
        $static = &$objects['decorations'];
        $dynamic = &$image_attributes[$tank]['decorations'];
        foreach($static as $decoration => $attributes) {
            if (!isset($dynamic[$decoration])) {
                continue;
            }
            $array[$tank]['decorations'][$decoration] = array_merge($static[$decoration], $dynamic[$decoration]);
        }
    }
}

$aqua_z_attributes = get_config_json($config['path_root'] . '/db/aqua-z.json');
mergeDecorations($aqua_z_images, $aqua_z_attributes);
?>
