<?php
/* Define images parts as an associative array of named relay outlets grouped by table classes.
   They names are the same as ones from json-file, where its values are associative arrays of
   image parts parameters.
   A list 'image' contains relay states images.
   parameter is used by 'javascript'.
 */
$images = [
    /* Class of table for a whole object image.
     * You can configure multiple classes for multuple controlled objects.
     */
    'aqua1' => [
        /* Row of table columns for parts of whole image, an image width and
         * a list of images of relay states (Image part with index 0 corresponds
         * to the state of the switch "on", and with index 1 - "off".
         * At least for my relay it is)
         */
        'outlet2' => [
            'column' => 0,
            'width' => 120,
            'image' => [
                'ON' => '/plugins/aqua/images/aqua1_light_additional.png',
                'OFF' => $image_blank
            ]
        ],
        'outlet1' => [
            'column' => 1,
            'width' => 140,
            'image' => [
                'ON' => '/plugins/aqua/images/aqua1_light_main.png',
                'OFF' => $image_blank
            ]
        ],
        'outlet3' => [
            'column' => 2,
            'width' => 120,
            'image' => [
                'ON' => '/plugins/aqua/images/aqua1_air_bubbles.png',
                'OFF' => $image_blank
            ]
        ]
    ],
    'aqua2' => [
        'outlet4' => [
            'column' => 0,
            'width' => 120,
            'image' => [
                'ON' => '/plugins/aqua/images/aqua2_light_main.png',
                'OFF' => $image_blank
            ]
        ],
        'empty' => [
            'column' => 1,
            'width' => 140
        ]
    ]
];
?>
