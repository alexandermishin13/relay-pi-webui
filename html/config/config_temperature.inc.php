<?php
/* Set for a images classes an IDs of thermometers if needed.
 * ID is an 'id' value from section 'thermometers' of 'relay.json'.
 */
$thermometers = [
    'therm1' => [
        'id' => 'aqua1',
        'decimals' => 1
    ],
    'therm2' => [
        'id' => 'aqua2',
        'decimals' => 1
    ]
];
?>
