<?php

namespace RelayPi\WebUI;

include_once $_SERVER['DOCUMENT_ROOT'].'/config/config.inc.php';

/* Probe for a language include with constants */
if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    $probelang = $config['document_root'] . '/languages/lang_' . $lang . '.inc.php';
    if (file_exists($probelang)) {
        include $probelang;
    }
} else {
    $lang = 'en';
}
include $config['document_root'] . '/languages/lang_en.inc.php';

function action_is_on($action) {
    return in_array(strtolower($action), [1, 'on', 'yes', 'y', 'enable', 'true']);
}

$relayName = $_GET['outlet'];

/* Try to load a config json */
Sensors::readConfig($config['path_root'] . '/' . $config['db_json']);

session_start();
header('Content-Type: text/html; charset=utf-8');

?>
<!DOCTYPE HTML>
<html lang="<?php echo $lang ?>">
<head>
<title data-l10n><?php echo $config['title_schedule'] ?></title>
<?php if ($config['meta_viewport']) { ?>
<meta name="viewport" content="<?php echo $config['meta_viewport'] ?>">
<?php } ?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="/styles/styles.css">
<link rel="stylesheet" href="/styles/edit.css">
</head>
<body>
<h1 data-l10n><?php echo $config['title'] ?></h1>
<section>
<hr>
<h2 data-l10n>Schedule</h2>
<?php
/* If entry is invalid print error and exit */
$relayObject = Sensors::getRelayObject($relayName);
if (!isset($relayObject)) {
    printf(ERROR_NOT_FOUND, $relayName);
?>
</body>
</html>
<?php
    exit();
}

/* Get system timers */
$timers = $relayObject->getTimers();

/* Enrich timers from the remote platform */
$timersRemote = $relayObject->getPlatformTimers();
foreach ($timersRemote as $timer) {
    $timer[4] = $relayObject->platformObject->platformName;
    $timers[] = $timer;
}
?>
<script>
let timers = <?php echo json_encode($timers) ?>;
<?php
if (isset($_SESSION['username'])) {
?>
const authenticated = true;
const relay = '<?php echo $relayName ?>';
<?php
} else {
?>
const authenticated = false;
<?php
}
$description = join(' ', array_map(function($value) {
    return '<span data-l10n>'.$value.'</span>';
}, $relayObject->descriptions));
?>
</script>
<section class="modal" id="edit_form"></section>
<table id="schedule">
<caption><?php echo get_name($relayName) ?> (<?php echo $description ?>)</caption>
</table>
<script src="/js/Dictionary.js"></script>
<script src="/js/Translation.js"></script>
<script src="/js/functions.js"></script>
<script type="module" src="/js/edit.js"></script>
</section>
</body>
</html>
