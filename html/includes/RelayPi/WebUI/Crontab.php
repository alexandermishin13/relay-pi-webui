<?php

declare(strict_types=1);

namespace RelayPi\WebUI;

/**
 * Crontab Class
 */
class Crontab {
    private array $list;
    private string $binary;

    /**
     * Constructor for Crontab Class
     */
    public function __construct()
    {
        $this->list = [];
        $this->binary = '/usr/bin/crontab';
    }

    /**
     * Adds Job object to Crontab collection
     *
     * @param   Job     $job    Job object
     */
    public function addJob(Crontab\Job $job): void
    {
        $this->list[] = $job;
    }

    /**
     * Adds text remark to Crontab collection
     *
     * @param   string  $remark Text string (w/o '#' neither EOL)
     */
    public function addRemark(string $remark): void
    {
        $this->list[] = '# ' . $remark;
    }

    /**
     * Returns all Crontab collection for a testing purpose
     *
     * @return  array  Array of jobs or remarks objects
     */
    public function all(): array
    {
        return $this->list;
    }

    /**
     * Saves a Crontab collection as a crontab file
     *
     * Clears a crontab file and install a new generated one
     */
    public function save(): void
    {
        /* Clear the job list */
        exec($this->binary . ' -r');
        /* Fill the job list line by line */
        $handle = popen($this->binary . ' -', 'w');
        foreach ($this->list as $element) {
            $str = is_string($element) ? $element : $element->text();
            fwrite($handle, $str . PHP_EOL);
        }
        pclose($handle);
    }
}

?>
