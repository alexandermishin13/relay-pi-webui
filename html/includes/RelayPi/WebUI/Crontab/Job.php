<?php

declare(strict_types=1);

namespace RelayPi\WebUI\Crontab;

/**
 * Job Class
 */

class Job {
    private string $minute;
    private string $hour;
    private string $mday;
    private string $mon;
    private string $wday;
    private string $command;
    private string $at;

    /**
     * Constructor for Job Class
     *
     * @param string    $command        Command to run
     */
    public function __construct()
    {
        $commands = [];
        foreach (func_get_args() as $command) {
            $commands[] = $command;
        }
        $this->command = escapeshellcmd(join(' ', $commands)) ?? '';
        $this->minute = $this->hour = $this->mday = $this->mon = $this->wday = '*';
        $this->at = '';
    }

    /**
     * Sets time for the job
     *
     * @param   string  $when   (Time 'hh:mm' or '@reboot')
     */
    public function setTime(string $when): void
    {
        $events = [
            '@reboot' => true,
        ];

        if (isset($events[$when])) {
            $this->at = $when;
            $this->minute = $this->hour = '*';
        } else {
            $time = date_parse($when);

            if ($time['minute'] !== false) {
                $this->minute = strval($time['minute']);
            } else {
                $this->minute = '*';
            }

            if ($time['hour'] !== false) {
                $this->hour = strval($time['hour']);
            } else {
                $this->hour = '*';
            }
        }
    }

    /**
     * Sets days of week for the job
     *
     * @param integer,
     *        string,
     *        array of integer $dows	0-7 from 'Sun' to 'Sun'
     */
    public function setDOW($dows): void
    {
        if (is_array($dows)) {
            $daylist = array_unique(array_map('intval', $dows));
            sort($daylist);
            // '*' if weekdays are not between 0 and 7. If...else order is important!!!
            if (($daylist[0] < 0) || ($daylist[count($daylist)-1] > 7)) {
                $this->wday = '*';
            } else {
                $this->wday = $this->orderedArrayToRange($daylist);
            }
        }
        elseif (is_integer($dows) && ($dows >= 0) && ($dows <= 7)) {
            $this->wday = strval($dows);
        } else {
            $this->wday = '*';
        }
    }

    /**
     * Convert Job to string for crontab file
     *
     * @return  string
     */
    public function text(): string
    {
        if ($this->at) {
            $job = [$this->at];
        } else {
            $job = [$this->minute, $this->hour, $this->mday, $this->mon, $this->wday];
        }
        $job[] = $this->command;

        return implode("\t", $job);
    }

    /**
     * Convert an ordered array to a string of ranges.
     *
     * @param   array   Ordered row of numbers
     * @return  string  Comma delimited list of ranges of numbers
     */
    private function orderedArrayToRange(array $array): string
    {
        $end = $start = $array[0];
        $ranges = [];

        foreach ($array as $number) {
            if ($number - $end > 1) {
                $ranges[] = ($start === $end) ? $start : $start . '-' . $end;
                $start = $number;
            }
            $end = $number;
        }
        $ranges[] = ($start === $end) ? $start : $start . '-' . $end;

        return implode(',', $ranges);
    }
}
?>
