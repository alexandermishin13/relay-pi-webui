<?php

declare(strict_types=1);

namespace RelayPi\WebUI;

/**
 * Block of all platforms of relays
 */

class Sensors
{
    public static $config = null;
    protected static array $platforms = [];
    protected static array $relays = [];
    protected static array $therms = [];

    /**
     * Reads a configuration file
     */
    public static function readConfig(string $configFile, bool $jsonError = false)
    {
        if (file_exists($configFile)) {
            self::$config = json_decode(file_get_contents($configFile), true);
            if (self::$config === null) {
                $errorMessage = sprintf(ERROR_CANNOT_DECODE_JSON, basename($configFile));
                print(($jsonError) ? "{'error':'invalidConfigFile','message':'$errorMessage'}" :  $errorMessage . PHP_EOL);

                exit(2);
            }
        } else {
            $errorMessage = sprintf(ERROR_CANNOT_OPEN_FILE, basename($configFile));
            print(($jsonError) ? "{'error':'noConfigFile','message':'$errorMessage'}" : $errorMessage . PHP_EOL);

            exit(1);
        }
    }

    /**
     * Construct and return a new object for a named platform by its config
     *
     * @param  string   $platformName
     * @param  array    $platformConfig
     * @return object   $platformObject
     */
    private static function platformObject(string $platformName, array $platformConfig): object
    {
        $platformType = $platformConfig['type'] ?? 'gpio';

        switch ($platformType) {
            case 'gpio':
                $platformObject = new Sensors\PlatformGpio($platformName);
                break;
            case 'tasmota':
                $platformHost = $platformConfig['host'] ?? '192.168.14.1'; // Tasmota defaults
                $platformObject = new Sensors\PlatformTasmota($platformName, $platformHost);
                break;
            default:
                $platformObject = (object)[
                    'error' => 'noPlatformConfigured',
                    'message' => sprintf(ERROR_PLATFORM_TYPE_WRONG, $platformType)
                ];
                break;
        }

        return $platformObject;
    }

    /**
     * Create newobject for the sensors patform if it not registered yet
     * and add it to self::platforms or get already registered one
     *
     * @param  string $platformName
     * @return object $platformObject
     *
     */
    public static function getPlatformObject(string $platformName, bool $jsonError = false): object
    {
        /* Register new platform if not already registered */
        if (!isset(self::$platforms[$platformName])) {
            /* return an error end exit if no platform name configured */
            if (!isset(self::$config['platforms'][$platformName])) {
                $errorMessage = sprintf(ERROR_PLATFORM_NOT_FOUND, $platformName);
                print(($jsonError) ? "{'error':'noPlatformConfigured','message':'$errorMessage'}" : $errorMessage . PHP_EOL);

                exit(3);
            }

            $platformConfig = & self::$config['platforms'][$platformName];
            $platformObject = self::platformObject($platformName, $platformConfig);

            /* If $platformObject->error is set, $platformObject contains nothing but error info */
            if (isset($platformObject->error)) {
                print(($jsonError) ? json_encode($platformObject) : $platformObject->message . PHP_EOL);

                exit(3);
            }

            self::$platforms[$platformName] = $platformObject;
        }

        return self::$platforms[$platformName];
    }

    /**
     * Construct and return a new object for a named relay by its config
     *
     * @param  string   $relayName
     * @param  array    $relayConfig
     * @return object   $relayObject
     */
    private static function relayObject(string $relayName, array $relayConfig): object
    {
        $platformName = $relayConfig['platform'] ?? 'local';

        /* Register a new relay object */
        $relayObject = new Sensors\SensorRelay(
            $relayName,
            $relayConfig,
            self::getPlatformObject($platformName)
        );

        return $relayObject;
    }

    /**
     * Construct and return a new object for a named thermometer by its config
     *
     * @param  string   $thermName
     * @param  array    $thermConfig
     * @return object   $thermObject
     */
    private static function thermObject(string $thermName, array $thermConfig): object
    {
        $platformName = $thermConfig['platform'] ?? 'local';

        /* Register a new thermometer */
        $thermObject = new Sensors\SensorThermometer(
            $thermName,
            $thermConfig,
            self::getPlatformObject($platformName)
        );

        return $thermObject;
    }

    /**
     * Register and return a new object for a relay by its name
     *
     * @param  string   $relayName
     * @return object   $relayObject
     */
    public static function getRelayObject(string $relayName, bool $jsonError = false): object
    {
        /* Register new relay if not already registered */
        if (!isset(self::$relays[$relayName])) {
            if (!isset(self::$config['outlets'][$relayName])) {
                $errorMessage = sprintf(ERROR_NOT_FOUND, $relayName);
                print(($jsonError) ? "{'error':'noRelayConfigured','message':'$errorMessage'}" : $errorMessage . PHP_EOL);

                exit(3);
            }

            /* Register new relay for its platform object */
            $relayObject = self::relayObject($relayName, self::$config['outlets'][$relayName]);
            self::$relays[$relayName] = $relayObject;
        }

        return self::$relays[$relayName];
    }

    /**
     * Register and return a new object for a thermometer by its name
     *
     * @param  string   $thermName
     * @return object   $thermObject
     */
    public static function getThermObject(string $thermName, bool $jsonError = false): object
    {
        /* Register new relay if not already registered */
        if (!isset(self::$therms[$thermName])) {
            if (!isset(self::$config['thermometers'][$thermName])) {
                $errorMessage = sprintf(ERROR_NOT_FOUND, $thermName);
                print(($jsonError) ? "{'error':'noThermometerConfigured','message':'$errorMessage'}" : $errorMessage . PHP_EOL);

                exit(3);
            }

            /* Register new relay for its platform object */
            $thermObject = self::thermObject($thermName, self::$config['thermometers'][$thermName]);
            self::$therms[$thermName] = $thermObject;
        }

        return self::$thermss[$thermName];
    }

    /**
     * Iterate through sensors of specified type for the specified platform or
     * for all platforms if $platformName ommited
     *
     * @param  string   $sensorsType
     * @param  string   $platformName or null
     * @return iterable pairs,
     *     string $sensorName => reference to $sensorConfig dictionary
     */
    public static function iterateSensorsPlatform(string $sensorsType, ?string $platformName = null): iterable
    {
        foreach (self::$config[$sensorsType] as $sensorName => &$sensorConfigRef) {
            if (!is_null($platformName) && ($platformName !== $sensorConfigRef['platform'] ?? 'local')) {
                continue;
            }
            yield $sensorName => $sensorConfigRef;
        }
    }

    /**
     * Iterate the all relays
     */
    public static function iterateRelaysAll(): iterable
    {
        foreach (self::iterateSensorsPlatform('outlets') as $relayName => $relayConfig) {
            $platformName = $relayConfig['platform'] ?? 'local';

            /* Register new relay for its platform object */
            $relayObject = new Sensors\SensorRelay(
                $relayName,
                $relayConfig,
                self::getPlatformObject($platformName)
            );
            yield $relayName => $relayObject;
        }
    }

    /**
     * Iterate through relays of the specified platform
     *
     * @param  string   $platformName
     * @return iterable pairs,
     *     string $relayName => object $relayObject
     */
    public static function iterateRelaysPlatform(string $platformName): iterable
    {
        foreach (self::iterateSensorsPlatform('outlets', $platformName) as $relayName => $relayConfig) {
            /* Register new relay for its platform object */
            $relayObject = new Sensors\SensorRelay(
                $relayName,
                $relayConfig,
                self::getPlatformObject($platformName)
            );
            yield $relayName => $relayObject;
        }
    }

    /**
     * Iterate through thermometers of the specified platform
     *
     * @param  string   $platformName
     * @return iterable pairs,
     *     string $thermName => object $thermObject
     */
    public static function iterateThermPlatform(string $platformName): iterable
    {
        foreach (self::iterateSensorsPlatform('thermometers', $platformName) as $thermName => $thermConfig) {
            /* Register new relay for its platform object */
            $platformObject = self::getPlatformObject($platformName);
            $thermObject = new Sensors\SensorThermometer(
                $thermName,
                $thermConfig,
                $platformObject
            );
            yield $thermName => $thermObject;
        }
    }

    /**
     * Switch the chosen relay to the state
     *
     * @param   string  $relayName
     * @param   string  $relayState     'ON', 'OFF', 'TOGGLE'
     * @return  string  $relayNewState  'ON', 'OFF' or null
     */
    public static function relaySwitch(string $relayName, string $relayState, bool $jsonError = false): ?string
    {
        $relayObject = self::getRelayObject($relayName, $jsonError);
        if ($relayObject->active) {
            $relayNewState = $relayObject->setState($relayState);
        } else {
            $relayNewState = null;
        }

        return $relayNewState;
    }

    /**
     * Gets a state of a named relay
     *
     * @param   string  $relayName
     * @return  string  $relayState     'ON', 'OFF' or null
     */
    public static function relayState(string $relayName, bool $jsonError = false): ?string
    {
        $relayObject = self::getRelayObject($relayName, $jsonError);
        if ($relayObject->active) {
            $relayState = $relayObject->getState();
        } else {
            $relayState = null;
        }

        return $relayState;
    }

    /**
     * Iterate through relays ot the chosen platform
     *
     * @param  string $platformName
     * @return iterable pairs,
     *     string $relayName => string $relayState ('ON', 'OFF' or null)
     */
    public static function iterateRelaysPlatformState(string $platformName, bool $jsonError = false): iterable
    {
        $iterateRelayPlatform = self::iterateRelaysPlatform($platformName, $jsonError);
        foreach ($iterateRelayPlatform as $relayName => $relayObject) {
            if (
                $relayObject->active
                && (($relayState = $relayObject->getState()) !== null)
            ) {
                yield $relayName => $relayState;
            }
        }
    }

    /**
     * Gets a state of a named relay
     *
     * @param   string  $thermName
     * @return  float   $thermState
     */
    public static function thermState(string $thermName, bool $jsonError = false): ?float
    {
        $thermObject = self::getThermObject($thermName);
        if ($thermObject->active) {
            $thermState = $thermObject->getTemperature() + $thermObject->correction;
        } else {
            $thermState = null;
        }

        return $thermState;
    }

    /**
     * Iterate through thermometers ot the chosen platform
     *
     * @param  string $platformName
     * @return iterable pairs,
     *     string $thermName => float $thermState
     */
    public static function iterateThermPlatformState(string $platformName, bool $jsonError = false): iterable
    {
        $iterateThermPlatform = self::iterateThermPlatform($platformName, $jsonError);
        foreach ($iterateThermPlatform as $thermName => $thermObject) {
            /* No results when sensor is no active or has no sensor data */
            if (
                $thermObject->active
                && (($temperature = $thermObject->getTemperature()) !== null)
            ) {
                yield $thermName => ($temperature + $thermObject->correction);
            }
        }
    }
}
?>
