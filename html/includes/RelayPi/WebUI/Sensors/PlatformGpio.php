<?php

declare(strict_types=1);

namespace RelayPi\WebUI\Sensors;

putenv('LC_NUMERIC=C');

/**
 * Block of GPIO based relays
 */
class PlatformGpio implements PlatformInterface
{
    public array $verbal = ['ON', 'OFF'];
    public string $platformName;
    public string $platformHost;
    public string $platformType = 'gpio';
    private string $gpio_set;
    private string $gpio_get;
    private array $systemInfo = [];
    private object $getThermOsDepended;

    public function __construct(string $platformName)
    {
        $this->platformName = $platformName;
        $hostName = php_uname('n');
        $ipAddress = gethostbyname($hostName);
        $this->platformHost = $hostName . ($hostName == $ipAddress ? '' : " [$ipAddress]");
        $this->systemInfo['Firmware'] = php_uname('s') . ' ' . php_uname('r');

        /* GPIO control utility
         * For Linux You need WiringPI (or WiringOP)
         * For FreeBSD You need nothing but system
         */
        switch (PHP_OS) {
            case 'Linux':
                /* '-1' - use a physical pin numbers */
                $this->gpio_set = '/usr/local/bin/gpio -1 write';
                $this->gpio_get = '/usr/local/bin/gpio -1 read';
                $this->model_get = '/usr/bin/cat /proc/device-tree/model';

                /* Get temperature by a Linux way */
                $this->getThermOsDepended = function ($romid): ?float {
                    $temperature = null;

                    $slave_path = '/sys/devices/w1_bus_master1/' . $romid . '/w1_slave';
                    if(file_exists($slave_path)) {
                        $matches = [];
                        $data = file($slave_path, FILE_IGNORE_NEW_LINES);
                        /* Get the temperature in 1/1000 of degree units */
                        if (preg_match("/t=(.*)/", $data[1], $matches))
                            $temperature = $matches[1] / 1000;
                    }

                    return $temperature;
                };
                break;
            case 'FreeBSD':
                /* '-p' - use a physical pin numbers */
                $this->gpio_set = '/usr/sbin/gpioctl -p';
                $this->gpio_get = $this->gpio_set;
                $this->model_get = '/sbin/sysctl -b hw.fdt.model';

                /* Get temperature by a FreeBSD way */
                $this->getThermOsDepended = function ($romid): ?float {
                    $temperature = null;

                    /* Prepare freebsd array if not prepared */
                    if (empty($this->resultTherm)) {
                        /* Read information from the branch 'dev.ow_temp' of the kernel variables tree
                         * but from only needed leafs.
                         */
                        $temp_get = '/sbin/sysctl dev.ow_temp | /usr/bin/grep -e "\(rom\|tempe\)"';

                        $handle = popen($temp_get, 'r');
                        while (!feof($handle)) {
                            $buffer = fgets($handle, 512);
                            if ($buffer === false)
                                continue;

                            $matches = [];
                            preg_match("/^dev\.ow_temp\.(\d+)\.([\w\%]+):\s*(.+)$/", $buffer, $matches);

                            $parts = [];
                            if ($matches[2] === '%pnpinfo') {
                                /* Strip the unused 'romid=' */
                                preg_match("/romid=(.+)/", $matches[3], $parts);
                                $this->resultTherm['romid'][$parts[1]] = $matches[1];
                            }
                            elseif ($matches[2] === 'temperature') {
                                /* Strip the trailing 'C' */
                                preg_match("/([\d\.\,]+)/", $matches[3], $parts);
                                $this->resultTherm['temperature'][$matches[1]] = (float) $parts[1];
                            }
                        }
                        pclose($handle);
                    }

                    /* Get the value from the prepared array */
                    $index = $this->resultTherm['romid'][$romid];
                    if (isset($index))
                        $temperature = $this->resultTherm['temperature'][$index];

                    return $temperature;
                };
                break;
            default:
                exit("I don't know how to use gpio on ".PHP_OS);
        }
        $this->systemInfo['Hardware'] = shell_exec($this->model_get);
    }

    /**
     * Get a pin state as an integer value.
     *
     * @param   int     the relay pin
     * @return  string  'ON', 'OFF' or null
     */
    private function pinState(int $pin): ?int
    {
        $output = [];
        /* Escape the only argument */
        $command = $this->gpio_get . ' ' . escapeshellarg((string) $pin);
        /* Run the externap gpio command to read a relay state and write a result to $level[0] */
        exec($command, $output, $result);
        if (($result !== 0) || (!isset($output[0]))) {
            return null;
        }

        return (integer) $output[0];
    }

     /**
      * Get a platform information
      *
      * @return  array
      */
     public function getInfo(): array
     {
         return $this->systemInfo;
     }

    /**
     * Get a relay state.
     *
     * @param   int     the relay pin
     * @return  string  'ON', 'OFF' or null
     */
    public function getRelay(int $pin): ?string
    {
        $state = $this->pinState($pin);

        return isset($state) ? $this->verbal[$state] : null;
    }

    /**
     * Set a relay to target state and return its value
     *
     * @param   int     the relay pin
     * @return  string  'ON', 'OFF' or null
     */
    public function setRelay(int $pin, string $target): ?string
    {
        switch (strtoupper($target)) {
            case 'ON':
                $state = 0;
                break;
            case 'OFF':
                $state = 1;
                break;
            case 'TOGGLE':
                $state = $this->pinState($pin);
                if ($state !== null) {
                    $state = abs(--$state);
                    break;
                }
                // No break
            default:
                return null;
        }

        $output = [];
        /* Escape first argument only. Second one is calculated integer, so it's ok */
        $command = $this->gpio_set . ' ' . escapeshellarg((string) $pin) . ' ' . $state;
        /* Run the external gpio command to switch relay */
        exec($command, $output, $result);
        if ($result !== 0) {
            return null;
        }

        return $this->getRelay($pin);
    }

    /**
     * Get temperature values one-wire connected sensors.
     *
     * @param   string  the thermometer romid
     * @return  float   temperature or null
     */
    public function getTherm(string $romid): ?float
    {
        $getTherm = $this->getThermOsDepended;
        $temperature = $getTherm($romid);

        return $temperature;
    }

    /**
     * Get platform timers for the relay (dummy)
     *
     * @param   int     the relay pin (not really used)
     * @return  array   always empty
     */
    public function getTimers(int $pin): array
    {
        return [];
    }
}
?>
