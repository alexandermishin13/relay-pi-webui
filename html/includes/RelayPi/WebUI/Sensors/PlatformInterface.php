<?php

declare(strict_types=1);

namespace RelayPi\WebUI\Sensors;

/**
 * Classes for blocks of relays
 */

interface PlatformInterface
{
    public function getInfo(): array;
    public function getRelay(int $pin): ?string;
    public function setRelay(int $pin, string $target): ?string;
    public function getTherm(string $romid): ?float;
    public function getTimers(int $pin): array;
}

?>
