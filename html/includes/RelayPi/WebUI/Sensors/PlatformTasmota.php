<?php

declare(strict_types=1);

namespace RelayPi\WebUI\Sensors;

/**
 * Block of remote relays with Tasmota firmware
 */
class PlatformTasmota implements PlatformInterface
{
    const RELAY = 'POWER';
    const THERM = 'DS18B20';
    const TIMERS = 'TIMERS';
    const RELAYSTATUS = 'POWER0';
    const THERMSTATUS = 'STATUS 10';
    const SYSTEMINFO = 'STATUS 2';

    public string $platformName;
    public string $platformHost;
    public string $platformType = 'tasmota';
    private string $url;
    private array $options;
    private array $weekDaysOrder = [1, 2, 3, 4, 5, 6, 0];
    private array $systemInfo = [];
    private object $resultRelay;
    private array $resultTherm = [];
    private array $resultTimers = [];
    private object $resultSysInfo;

    public function __construct(string $platformName, string $platformHost = '192.168.14.1')
    {
        $this->platformName = $platformName;
        $this->platformHost = $platformHost;
        $this->url = 'http://' . $platformHost . '/cm';
        /* Short timeout for unreachable devices */
        $this->options = [
            'http'=> [
                'method' => 'POST',
                'timeout' => 2
            ]
        ];
    }

    /**
     * Convert the Tasmota representation of configured week days to array
     *
     * @param   string  Seven 0|1 characters string for configured days of week
     * @return  array   Variable length array for configured days only
     */
    private function convertWeekDays(string $weekDays): array
    {
        $week = [];
        for ($day = 0; $day < 7;) {
            if ($weekDays[$this->weekDaysOrder[$day++]] === '1') {
                $week[] = $day;
            }
        }
        return $week;
    }

    /**
     * Send command to remote "tasmota" device.
     *
     * @param   string  Cmnd text
     * @return  string  Json result of the request
     */
    private function sendCmnd(string $cmnd): string
    {
        $options = $this->options;
        $options['http']['content'] = http_build_query(['cmnd' => $cmnd]);
        $context = stream_context_create($options);
        $json = @file_get_contents($this->url, false, $context) ?: '{}';

        return $json;
    }

    /**
     * Get a platform information
     *
     * @return  array
     */
    public function getInfo(): array
    {
        if (!$this->systemInfo) {
            $this->resultSysInfo = json_decode($this->sendCmnd(self::SYSTEMINFO));

            $info = $this->resultSysInfo->StatusFWR;
            if ($info->Hardware)
                $this->systemInfo['Hardware'] = $info->Hardware;
            if ($info->Firmware)
                $this->systemInfo['Firmware'] = $info->Version;
        }
        return $this->systemInfo;
    }

    /**
     * Get a relay state.
     *
     * @param   int     the relay pin
     * @return  string  'ON', 'OFF' or null
     */
    public function getRelay(int $pin): ?string
    {
        $key = self::RELAY . $pin;

        if (!isset($this->resultRelay->$key)) {
            $json = $this->sendCmnd(self::RELAYSTATUS);
            $this->resultRelay = json_decode($json);
            if (!isset($this->resultRelay->$key)) {
                $this->resultRelay->$key = 'OFF';
            }
        }
        $state = $this->resultRelay->$key;

        return $state;
    }

    /**
     * Set a relay to target state and return its value
     *
     * @param   int     the relay pin
     * @return  string  'ON', 'OFF' or null
     */
    public function setRelay(int $pin, string $target): ?string
    {
        $key = self::RELAY . $pin;

        $result = json_decode($this->sendCmnd($key . ' ' . $target));
        /* Sync the cached results */
        $state = $result->$key;
        $this->resultRelay->$key = $state;

        return $state;
    }

    /**
     * Get temperature from remote tasmota device
     *
     * @param   string  the thermometer romid
     * @return  float   temperature or null
     */
    public function getTherm(string $romid): ?float
    {
        /* Just return the temperature if it already exists */
        if (array_key_exists($romid, $this->resultTherm)) {
            return $this->resultTherm[$romid];
        }

        $json = $this->sendCmnd(self::THERMSTATUS);
        $result = json_decode($json, true);

        /* NULL if no results */
        $temperature = null;

        if (!isset($result['StatusSNS']) || !is_array($result['StatusSNS'])) {
            return $temperature;
        }

        $status10 = $result['StatusSNS'];

        foreach ($status10 as $key => $therm) {
            /* Check if the property name starts with 'DS18B20' and its value is array */
            if (!is_array($therm) || (strpos($key, self::THERM, 0) !== 0)) {
                continue;
            }

            $rid = $therm['Id'];
            $this->resultTherm[$rid] = $therm['Temperature']; // Cache the result

            /* The temperature value is found. No needs to cache it */
            if ($rid === $romid) {
                $temperature = $this->resultTherm[$rid];
            }
        }

        /* The value or null when the romid is not exists */
        return $temperature;
    }

    /**
     * Get the platform timers
     */
    private function getPlatformTimers()
    {
        $json = $this->sendCmnd(self::TIMERS);
        $timers = json_decode($json, false);

        /* Return empty array if timers is not enebled */
        $this->resultTimers = [];
        if ($timers->Timers !== 'ON') {
            return;
        }
        /* Walk through timers and fill the resulted array */
        foreach ($timers as $timerName => $timerObject) {
            if (!is_object($timerObject) || ($timerObject->Enable === 0)) {
                continue;
            }
            /* Pin => [ [Date", "Action", [enabled,days,of,week]], ...] */
            $timer = [
                $timerObject->Time,
                ($timerObject->Action === 1) ? 'on' : 'off',
                $this->convertWeekDays($timerObject->Days),
                $timerObject->Mode
            ];

            $this->resultTimers[$timerObject->Output][] = $timer;
        }
    }

    /**
     * Get platform timers for the relay
     *
     * @param   int     the relay pin
     * @return  array   of the relay timers
     */
    public function getTimers(int $pin): array
    {
        /* Get all platform timers if not found any for the relay */
        if (!array_key_exists($pin, $this->resultTimers)) {
            $this->getPlatformTimers();
        }

        return $this->resultTimers[$pin] ?: [];
    }
}
?>
