<?php

declare(strict_types=1);

namespace RelayPi\WebUI\Sensors;

/**
 * Block of GPIO based relays
 */
class SensorRelay
{
    /* Absolute time of day */
    const TIMERMODE = 0;

    public bool $active;
    public object $platformObject;
    public array $timers;
    public int $pin;
    public string $description;
    public array $descriptions;

    private array $timersLocal;

    public function __construct(string $sensorName, array $sensorConfig, object $platform)
    {
        $this->pin = $sensorConfig['pin'] ?? 0;
        $this->active = $sensorConfig['active'] ?? true;
        $this->timers = $sensorConfig['timers'] ?? [];
        $this->descriptions = (array) ($sensorConfig['description'] ?? $sensorName);
        $this->description = join(' ', $this->descriptions);
        $this->platformObject = $platform;
    }

    /**
     * Set the relay to the new state by its platform dependent method
     *
     * @return string  'ON', 'OFF' or null
     */
    public function setState(string $newState): ?string
    {
        return $this->platformObject->setRelay($this->pin, $newState);
    }

    /**
     * Get the relay state by its platform dependent method
     *
     * @return string  'ON', 'OFF' or null
     */
    public function getState(): ?string
    {
        return $this->platformObject->getRelay($this->pin);
    }

    /**
     * Get local timers for the relay
     *
     * @return array    of scheduled actions
     */
    public function getTimers(): array
    {
        $this->timersLocal = [];
        foreach ($this->timers as $relayTimer) {
            /* Get week days array */
            $weekdays = $relayTimer['weekday'] ?? range(1, 7);
            $this->timersLocal[] = [$relayTimer['time'], $relayTimer['action'], $weekdays, self::TIMERMODE];
        };

        return $this->timersLocal;
    }

    /**
     * Get platform timers for the relay
     *
     * @return array    of scheduled actions
     */
    public function getPlatformTimers(): array
    {
        return $this->platformObject->getTimers($this->pin) ?: [];
    }
}
