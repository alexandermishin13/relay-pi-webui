<?php

declare(strict_types=1);

namespace RelayPi\WebUI\Sensors;

/**
 * Block of GPIO based relays
 */
class SensorThermometer
{
    public bool $active;
    public object $platformObject;
    public string $romid;
    public float $correction;
    public string $description;
    public array $descriptions;

    public function __construct(string $sensorName, array $sensorConfig, object $platform)
    {
        $this->romid = $sensorConfig['romid'] ?? '';
        $this->active = $sensorConfig['active'] ?? true;
        $this->correction = $sensorConfig['correction'] ?? 0;
        $this->platformObject = $platform;
        $this->descriptions = (array) ($sensorConfig['description'] ?? $sensorName);
        $this->description = join(' ', $this->descriptions);
    }

    /**
     * Get the thermometer value by its platform dependent method
     *
     * @return float    temperature
     */
    public function getTemperature(): ?float {
        return $this->platformObject->getTherm($this->romid);
    }

}
