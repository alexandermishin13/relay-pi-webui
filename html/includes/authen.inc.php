<?php

namespace RelayPi\WebUI;

function syslogAuth($message, $username) {
    global $config;

    if ($config['syslog']) {
        $ipaddr = $_SERVER['REMOTE_ADDR'];
        openlog('relay-pi', LOG_PID, LOG_AUTH);
        syslog(LOG_INFO, $message . ' ' . $username . ' from ' . $ipaddr);
        closelog();
    }
}

$auth = Sensors::$config['system']['auth'];
if (isset($auth)) {
    /**
     * If 'logout' a session is not needed anymore
     * If 'login' set the sessions variable 'username'
     */
    switch ($_POST['action'] ?? '') {
        case 'logout':
            unset($_SESSION['username']);
            break;
        case 'login':
            /* If plain text password is denied (good idea) */
            $allow_plaintext = Sensors::$config['system']['plaintext'] ?? true;
            /* If we have enough data to check the password */
            if (
                ($username = $_POST['username'] ?? false) &&
                ($jsonAuth = $auth[$username] ?? false) &&
                ($password = $_POST['password'] ?? false)
            ) {
                /* Check the hash. If not, check the plain text password
                   if it is allowed
                 */
                if (password_verify($password, $jsonAuth) || (($allow_plaintext) && ($jsonAuth === $password)))
                {
                    syslogAuth('Accepted password for', $username);
                    $_SESSION['username'] = $username;
                    break;
                } else {
                    syslogAuth('Failed password for invalid user', $username);
                }
            } else {
                syslogAuth('Invalid user', $username);
            }
            /* Pause if not authenticated */
            sleep(3);
            break;
        default:
            break;
    }

    if ($config['demo_mode']) {
?>
<div class="auth"><span id="demoMode" data-l10n>Demo Mode</span></div>
<?php
    } elseif (isset($_SESSION['username'])) {
        /* If logged in allows to switch relays and
         * writes a username and a logout prompt
         */
?>
<form method="POST" action="/">
<input type="hidden" name="action" value="logout">
</form>
<div class="auth"><span id="userName"><?php echo $_SESSION['username'] ?></span>
|<a id="auth_logout" target="_self"><?php echo AUTH_LOGOUT ?></a>
</div>
<?php
    } else {
        /**
         * If not logged in a click on buttons opens
         * a window with a login form which is normally hidden by css
         */
?>
<div class="modal" id="auth_form">
<div class="modal-header" id="header">
<span class="modal-close" id="auth_close">&times;</span>
<?php echo AUTH_LOGIN ?>
</div>
<hr>
<form method="POST" action="/">
<table>
<tr><td><?php echo USERNAME ?>:</td><td><input type="text" name="username"></td></tr>
<tr><td><?php echo PASSWORD ?>:</td><td><input type="password" name="password"></td></tr>
<tr><td></td><td><input type="submit" value=<?php echo AUTH_LOGIN ?>> <input type="reset"></td></tr>
</table>
<input type="hidden" name="action" value="login">
</form>
</div>
<div class="auth"><a id="auth_open"><?php echo AUTH_LOGIN ?></a></div>
<?php
    }
}
?>
<script src="/js/authen.js" defer></script>
