<?php

namespace RelayPi\WebUI;

include_once $_SERVER['DOCUMENT_ROOT'] . '/config/config.inc.php';
include_once $config['document_root'] . '/config/config-modules.inc.php';

/* Probe for a language include with constants */
if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    $probelang = $config['document_root'] . '/languages/lang_' . $lang . '.inc.php';
    if (file_exists($probelang)) {
        include $probelang;
    }
} else {
    $lang = 'en';
}
include $config['document_root'] . '/languages/lang_en.inc.php';

/* Set, or guess then set the timezone offset (ms) */
$tz = @timezone_open($config['timezone']) ?: @timezone_open(date_default_timezone_get());
$tz_offset = date_offset_get(date_create('now', $tz)) * 1000;

session_start();
header('Content-Type: text/html; charset=utf-8');

/* Try to load a config json */
Sensors::readConfig($config['path_root'] . '/' . $config['db_json'], true);
$dbJson = & Sensors::$config;

/* Include styles of modules */
$styles = [
    '/styles/styles.css',
    '/styles/footer.css',
    '/styles/switches.css'
];
foreach ($modules as $name => $module) {
    if ($module['active']) {
        foreach($module['style'] as $file) {
            array_push($styles, '/plugins/' . $name . '/' . $file);
        }
    }
}
?>
<!DOCTYPE HTML>
<html lang="<?php echo $lang ?>">
<head>
<title data-l10n><?php echo $config['title'] ?></title>
<?php if ($config['meta_viewport']) { ?>
<meta name="viewport" content="<?php echo $config['meta_viewport'] ?>">
<?php }
if ($config['meta_description']) { ?>
<meta name="description" content="<?php echo $config['meta_description'] ?>">
<?php
}
/* Include style files */
foreach ($styles as $file) {
?>
<link rel="preload" as="style" href="<?php echo $file ?>" onload="this.onload=null;this.rel='stylesheet'">
<?php
}
if ($config['demo_mode']) {
?>
<link rel="preload" as="style" href="/styles/demo.css" onload="this.onload=null;this.rel='stylesheet'">
<?php
}
?>
<noscript>
<?php
/* Include style files */
foreach ($styles as $file) {
?>
<link rel="stylesheet" href="<?php echo $file ?>">
<?php
}
?>
</noscript>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<script src="/js/functions.js" async></script>
<script src="/js/Chart.js" defer></script>
<script src="/js/Dictionary.js" async></script>
</head>
<body>
<?php
/* Generate login- or logout-form. On submit do its job
 */
include_once $config['document_root'].'/includes/authen.inc.php';
?>
<h1 data-l10n><?php echo $config['title'] ?></h1>
<section id="section_switchboard" class="collapsible">
<hr><h2 data-l10n>Switches</h2>
<div class="switchboard">
<?php
$timers = [];
$r_platforms = [];
$relayIterator = Sensors::iterateRelaysAll();
foreach ($relayIterator as $relayName => $relayObject) {
    if (!$relayObject->active) {
        continue;
    }
    /* Prepare timers for drawCharts() */
    $timers[$relayName] = $relayObject->getTimers();
    $platform = $relayObject->platformObject->platformName;
    $relayState = $relayObject->getState();
    $descrId = $relayName . '_descr';
    $r_platforms[$platform] = $r_platforms[$platform] ?? true;
    $description = join(' ', array_map(function($value) {
        return '<span data-l10n>'.$value.'</span>';
    }, $relayObject->descriptions));
?>
<div data-relay-name="<?php echo $relayName ?>">
<div id="<?php echo $descrId ?>" class="description"><?php echo $description ?></div>
<div class="schedule"></div>
<span class="state"><label class="switch"><input type="checkbox" id="<?php echo $relayName ?>"<?php echo ($relayState==='ON') ? ' checked="checked"' : '' ?> aria-labelledby="<?php echo $descrId ?>"><span class="slider <?php echo $config['switch_style'] ?>"></span></label></span>
</div>
<?php
}
?>
</div>
</section>
<script async>
const tz = <?php echo $tz_offset ?>;
const demoMode=<?php echo $config['demo_mode']?'true':'false' ?>;
const chartColor=<?php echo json_encode($chart_color) ?>;
const chartFormat=<?php echo json_encode($chart_format) ?>;
const relayPlatforms = <?php echo json_encode(array_keys($r_platforms)) ?>;
const outletsTimers=<?php echo json_encode($timers) ?>;
</script>
<?php
/* Include files of modules */
foreach ($modules as $name => $module) {
    if ($module['active']) {
        include_once $config['document_root'].'/plugins/'.$name.'/'.$module['code'];
    }
}
?>
<script id="final_job" type="module" src="/js/main.js" defer></script>
<footer>
<?php
include_once $config['document_root'].'/includes/footer.inc.php';
?>
</footer>
</body>
</html>
