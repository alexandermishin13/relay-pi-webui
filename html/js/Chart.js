class Chart {
  static xmlns = 'http://www.w3.org/2000/svg';
  /* Default values */
  static offsetLeft = 1.5;
  static width = 250;
  static height = 33;
  static fontSize = 7;
  static lineWidth = 2;
  static chartScale = true;
  static timePointer = true;
  /* Trying to calculate cells parameters from the chart dimensions */
  static cellWidth = Math.floor(this.width / 24);
  static cellHeight = Math.floor((this.height - this.fontSize) / 7);
  /* Data points for 7 schedules, one per weekday */
  data = [ [], [], [], [], [], [], [] ];
  constructor(timers) {
    this.element = document.createElementNS(this.constructor.xmlns, 'svg');
    this.element.setAttribute('fill', 'none');
    this.element.setAttribute('width', this.constructor.width + 'px');
    this.element.setAttribute('height', this.constructor.height + 'px');
    this.element.setAttribute('stroke-width', this.constructor.lineWidth + 'px');
    this.drawScale();
    for (const timer of timers) {
      const time = timer[0];
      const action = timer[1].toUpperCase();
      /* Weekday can be an array of weekdays or just a weekday number */
      const weekdays = this.constructor.getWeekdays(timer[2]);
      for (const day of weekdays) {
        this.addDataPoint(day, action, time);
      }
    }
    this.drawSchedule();
    this.drawTimePointer();
  }
  /*
   * Set multiple attributes for SVG
   */
  static setSvgAttributes(element, attributes) {
    for (const [key, value] of Object.entries(attributes)) {
      element.setAttributeNS(null, key, value);
    }
  }
  /*
   * A named SVG-definition for time pointers
   */
  static timePointerTemplate(msSinceMidnight) {
    const xmlns = this.xmlns;
    const defs = document.createElementNS(xmlns, 'defs');
    const pointer = document.createElementNS(xmlns, 'rect');
    const animation1 = document.createElementNS(xmlns, 'animate');
    const animation2 = document.createElementNS(xmlns, 'animate');
    const scaleWidth = 24 * this.cellWidth - 0.5;
    const scaleLeft = this.offsetLeft - 0.5;
    const x = msSinceMidnight * this.cellWidth / 3600000 + scaleLeft;
    this.setSvgAttributes(pointer, {
      id: 'chartTimePointer',
      x: scaleLeft,
      y: 0,
      width: 1,
      height: '100%',
      fill: 'crimson',
      'stroke-width': 0
    });
    this.setSvgAttributes(animation1, {
      attributeType: 'XML',
      attributeName: 'x',
      from: x,
      to: scaleWidth,
      dur: (86400 - msSinceMidnight / 1000) + 's',
      id: 'tpAnimation1'
    });
    this.setSvgAttributes(animation2, {
      attributeType: 'XML',
      attributeName: 'x',
      begin: "tpAnimation1.end",
      from: scaleLeft,
      to: scaleWidth,
      dur: '86400s',
      repeatCount: 'indefinite'
    });
    pointer.append(animation1, animation2);
    defs.appendChild(pointer);
    return defs;
  }
  /*
   * A named chart scale definition
   */
  static chartScaleTemplate() {
    const xmlns = this.xmlns;
    const cellWidth = this.cellWidth;
    const cellHeight = this.cellHeight;
    const defs = document.createElementNS(xmlns, 'defs');
    const chartCell = document.createElementNS(xmlns, 'pattern');
    const cellGrid = document.createElementNS(xmlns, 'path');
    const chartScale = document.createElementNS(xmlns, 'g');
    const scaleGrid = document.createElementNS(xmlns, 'rect');
    const labels = document.createElementNS(xmlns, 'text');
    this.setSvgAttributes(cellGrid, {
      d: 'M0,0H' + cellWidth + 'V' + cellHeight,
      fill: 'none',
      stroke: 'lightgray',
      'stroke-width': .75
    });
    this.setSvgAttributes(chartCell, {
      id: 'chartScaleGrid',
      x: 1.25,
      y: 1.25,
      width: cellWidth,
      height: cellHeight,
      patternUnits: 'userSpaceOnUse'
    });
    this.setSvgAttributes(chartScale, {
      id: 'chartScale'
    });
    this.setSvgAttributes(scaleGrid, {
      fill: 'url(#chartScaleGrid)',
      x: this.offsetLeft,
      y: 1.5,
      width: 24 * cellWidth,
      height: 7 * cellHeight,
      stroke: 'lightgray',
      'stroke-width': 1
    });
    this.setSvgAttributes(labels, {
      x: 0,
      y: 29,
      fill: 'gray',
      'font-family': 'sans-serif',
      'font-size': this.fontSize,
      'text-anchor': 'middle'
    });
    for (let i = 1; i <= 24; i++) {
      const thour = document.createElementNS(xmlns, 'tspan');
      const x = i * cellWidth + 1;
      thour.setAttribute('x', x);
      thour.textContent = i;
      labels.appendChild(thour);
    }
    chartCell.appendChild(cellGrid);
    chartScale.append(scaleGrid, labels);
    defs.append(chartCell, chartScale);
    return defs;
  }
  /*
   * Draw a chart scale if enabled
   */
  drawScale() {
    let chartScale = this.constructor.chartScale;
    /* If not disabled */
    if (chartScale) {
      /* If enabled but not generated yet */
      if (chartScale === true) {
        const csTemplate = this.constructor.chartScaleTemplate();
        this.element.appendChild(csTemplate);
        chartScale = document.createElementNS(this.constructor.xmlns, 'use');
        chartScale.setAttributeNS(null, 'href', '#chartScale');
        /* Generate a use-href command for content */
        this.constructor.chartScale = chartScale;
      }
      this.element.appendChild(chartScale.cloneNode());
    }
  }
  /*
   * Draw an animated timePointer if enabled
   */
  drawTimePointer() {
    let timePointer = this.constructor.timePointer;
    /* If not disabled */
    if (timePointer) {
      /* If enabled but not generated yet */
      if (timePointer === true) {
        const d = new Date((new Date()).getTime() + tz), msSinceMidnight = (d.getTime() - d.setUTCHours(0,0,0,0));
        const c = this.constructor;
        const tpTemplate = c.timePointerTemplate(msSinceMidnight);
        this.element.appendChild(tpTemplate);
        timePointer = document.createElementNS(c.xmlns, 'use');
        timePointer.setAttributeNS(null, 'href', '#chartTimePointer');
        /* Generate a use-href command for content */
        c.timePointer = timePointer;
      }
      this.element.appendChild(timePointer.cloneNode());
    }
  }
  /*
   * Draw the time lines
   */
  drawSchedule() {
    for (let i = 0; i < this.data.length; i++) {
      const y = i * this.constructor.cellHeight + 3;
      let data = '';
      for (const xPoint of this.data[i]) {
        data += 'M' + xPoint.ON + ',' + y + 'H' + xPoint.OFF;
      }
      if (data) {
        const path = document.createElementNS(this.constructor.xmlns, 'path');
        path.setAttribute('stroke', chartColor[i]);
        path.setAttribute('d', data);
        this.element.appendChild(path);
      }
    }
  }
  addDataPoint(day, action, time) {
    const i = day - 1;
    const t = time.split(':', 2);
    const point = t[0] * this.constructor.cellWidth + Math.floor(t[1] / 6) + this.constructor.offsetLeft;
    if (action === 'ON' || action.startsWith('MODE')) {
      this.data[i].push({['ON']:point});
    } else {
      this.data[i][this.data[i].length-1]['OFF'] = point;
    }
  }
  /*
   * Get weekdays as array
   */
  static getWeekdays(list) {
    const weekdays = Array.isArray(list) ? list : [list];
    const days = Array.from(new Set(weekdays.reduce((accum, day) => {
      if (Number.isInteger(day) && day >= 0 && day <= 7) {
        /* No strict for empty values */
        if (day == 0) day = 7;
        accum.push(day);
      }
      return accum;
    }, [])));
    /* If after all the days[] is still empty fill it by all 7 days */
    return (days.length > 0) ? days : [1, 2, 3, 4, 5, 6, 7];
  }
  /*
   * Draw time charts for all registered sources
   */
  static async drawCharts(outlets) {
    for (const [outlet, timers] of Object.entries(outlets)) {
      const chart = new Chart(timers);
      const chartContainer = document.querySelector('[data-relay-name="' + outlet + '"] .schedule');
      chartContainer.appendChild(chart.element);
    }
  }
}

/* Draw a time chart for an outlet */
// Chart.width = 250;
// Chart.height = 33;
// Chart.fontSize = 7;
// Chart.lineWidth = 2;
// Chart.chartScale = true;
// Chart.timePointer = true;
Chart.drawCharts(outletsTimers);
