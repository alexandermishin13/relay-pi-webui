class Dictionary {
  locale = 'en';
  dictionary = {};
  constructor(locale = navigator.language) {
    this.locale = locale;
  }
  async load(path = '/languages/lang_', locale = this.locale) {
    const url = path + locale + '.json';
    const request = new Request(url, {
      headers: {'Accept': 'application/json;charset=utf-8'}
    });
    await fetch(request)
      .then(response => {
        if (!response.ok) {
          return Promise.reject(response);
        }
        return response.json();
      })
      .then(data => {
        Object.assign(this.dictionary, data);
      })
      .catch(error => {
        console.log(error.status, error.statusText);
      });
  }
  gettext(msgid) {
    return this.dictionary[msgid] ?? msgid;
  }
}

const dict = new Dictionary;

function _ (msgid) {
  return dict.gettext(msgid);
}
