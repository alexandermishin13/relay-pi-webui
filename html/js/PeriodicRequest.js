export default class PeriodicRequest {
  handlers = [];
  platforms = {};
  static instances = {};
  static finalJobs = [];
  /*
   * Creates a new periodic request for sensors type
   */
  static registerRequestType(typeName, url, period) {
    return (new PeriodicRequest(typeName, url, null, period));
  }
  /*
   * Creates a new periodic task
   */
  static registerTaskType(typeName, mainHandler, period) {
    return (new PeriodicRequest(typeName, null, mainHandler, period));
  }
  /*
   * Run all final jobs
   */
  static startAll() {
    for (const job of PeriodicRequest.finalJobs) {
      job();
    }
    for (const [name, platformPeriodicTask] of Object.entries(PeriodicRequest.instances)) {
      platformPeriodicTask.start();
    }
  }
  constructor(name, url, mainHandler, period = 10000) {
    this.name = name;
    this.period = period;
    /* This is a request, not a task, if the URL is defined */
    if ((typeof url === 'string') && url) {
      this.template = new Request(url, {
        method: 'POST',
        cache: 'no-cache',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      });
      this.requestPlatform = async (platform) => {
        /* Request periodically platform's data and handle it */
        const data = await fetchJson( new Request(
          this.template,
          { body: platform.requestParameters }
        ));
        for (const callback of this.handlers) {
          callback(data);
        }
        platform.timer = window.setTimeout(this.requestPlatform, platform.period, platform);
      }
    } else {
      this.mainHandler = mainHandler;
      /* Run periodically platform's tasks */
      this.requestPlatform = async (platform) => {
        const data = this.mainHandler(platform);
        for (const callback of this.handlers) {
          callback(data);
        }
        platform.timer = window.setTimeout(this.requestPlatform, platform.period, platform);
      }
    }
    this.constructor.instances[name] = this;
  }
  /*
   * Add a handler of the periodic request's data to the queue
   */
  addHandler(handler) {
    if (typeof handler === 'function') {
      this.handlers.push(handler);
    } else {
      console.log('Handler must be a function. Ignored');
    }
  }
  /*
   * Register a new named platform
   */
  registerPlatform(name, params = {}) {
    this.platforms[name] = {
      name: name,
      period: this.period,
      requestParameters: JSON.stringify(params)
    };
  }
  /*
   * Run periodic tasks for all platforms this type
   */
  start() {
    for (const [name, platform] of Object.entries(this.platforms)) {
      platform.timer = window.setTimeout(this.requestPlatform, 0, platform); // start new task
    }
  }
}
