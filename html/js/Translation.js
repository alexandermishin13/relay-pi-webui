class Translation extends Dictionary {
  async load(path = '/js/lang/') {
    const locales = ['en', this.locale].filter((x, i, a) => a.indexOf(x) === i);
    for(const locale of locales) {
      await super.load(path, locale);
    }
  }
  get object() {
    return this.dictionary;
  }
}

const t9n = new Translation;
