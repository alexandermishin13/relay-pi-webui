const formAuth = document.getElementById('auth_form');
const buttonAuthOpen = document.getElementById('auth_open');
const buttonLogout = document.getElementById('auth_logout');

/* Just for visual effects, don't put too much faith in it */
let authenticated = false;

/* Open authentication form */
function authOn(event) {
  const element = event.target
  if (element.type === 'checkbox') {
    element.checked = element.getAttribute('checked');
  }
  formAuth.style.display = 'block';
}

/* set authentication button's handles on a click event */
if (buttonAuthOpen !== null) {
  const buttonAuthClose = document.getElementById('auth_close');
  const headerAuth = document.getElementById('header');

  /* Open and close an authentication window */
  buttonAuthOpen.addEventListener('click', function(event) {
    authOn(event, formAuth);
  });
  buttonAuthClose.addEventListener('click', function() {
    formAuth.style.display = 'none';
  });
  /* Make the form draggable by its header */
  setMovable(formAuth, headerAuth);

} else if (buttonLogout !== null) {
  /* It seems that user is authenticated */
  authenticated = true;
  /* Logout on click if logged in */
  buttonLogout.addEventListener('click', function() {
    document.forms[0].submit();
  });
}
