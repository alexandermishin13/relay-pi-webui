const saveUrl = '/requests/schedule_save.php';
const commandIcons = ['+', '✎', '✕'];
const timeMode = ['', 'sunrise', 'sunset'];

const formEdit = document.getElementById('edit_form');
const table = document.getElementById('schedule');

let cellEditTime;
let cellEditAction;
let cellEditDows;
let rowIndex = 0;

/*
 * Create the schedule
 */
async function createSchedule() {
  /* Add a header for a schedule table */
  const head = table.createTHead().insertRow(0);
  head.appendChild(document.createElement('th'));
  head.appendChild(document.createElement('th'));
  head.appendChild(document.createElement('th'));
  head.children[0].innerText = _('Time');
  head.children[1].innerText = _('Action');
  head.children[2].innerText = _('Weekdays');
  /* Add the OK and Cancel buttons */
  const buttons = document.createElement('p');
  const buttonSave = document.createElement('button');
  const buttonAway = document.createElement('button');

  buttonSave.innerText = _('Save');
  if (authenticated) {
    buttonSave.addEventListener('click', handleSaveButton);
  } else {
    buttonSave.disabled = true;
  }
  buttonAway.innerText = _('Cancel');
  buttonAway.addEventListener('click', redirectToMain);

  buttons.classList.add('buttons');
  buttons.append(buttonSave, ' ', buttonAway);
  table.parentNode.insertBefore(buttons, table.nextSibling);
}
/*
 * Clear and refill the schedule table with data
 */
async function populateSchedule() {
  clearSchedule();
  timers.sort(compareFirstColumn);
  for (const timer of timers) {
    /* Add a row for a timer record */
    populateRow(timer);
  }
  /* Add a row with an 'Add' button to the bottom */
  populateRow();
}
/*
 * Add command icon to the right of the schedule
 */
async function addCommandCell(row, command, enabled = true) {
  const cell = row.insertCell(-1)

  if (command === 0)
    cell.setAttribute('colSpan', 2); // Two cells for an 'Add' button

  if (Number.isInteger(command) && command >= 0 && command <= 2) {
    cell.appendChild(document.createTextNode(commandIcons[command]));
    cell.classList.add('command');
    if (enabled) {
      cell.dataset.command = command;
      cell.title = t9n.object.commands[command];
      cell.addEventListener(
        'click', handleEditForm, passiveSupported ? { passive: true } : false
      );
    } else {
      cell.classList.add('disabled');
    }
  }
}
/*
 * Fills a schedule table's row with data
 */
async function populateRow(timer = []) {
  const row = table.insertRow(-1);
  const cellTime = row.insertCell(-1);
  const cellAction = row.insertCell(-1);
  const cellDows = row.insertCell(-1);

  /* If it is a new entry (Add button) */
  if (timer.length === 0) {
    /* No controls for unauthenticated users */
    if (!authenticated) return;

    /* Add an 'Add' button */
    addCommandCell(row, 0); // Icon Add
  } else {
    /* Add leading zero to time parts */
    timer[0] = String(timer[0]).split(':').map(t => t.padStart(2, '0')).join(':');
    /* Decorate a timer row if it's time mode is not zero */
    if (timer[3] > 0) {
      const time = timer[0]
      const mode = timeMode[timer[3]];
      row.classList.add(mode); // A class
      cellTime.setAttribute('title', t9n.object.timemode[timer[3]]); // A tooltip
      if (time[0] >= '0' && time[0] <= '9') {
        timer[0] = '+' + time; // A leading plus sign
      }
    }
    /* Fill data cells */
    cellTime.appendChild(document.createTextNode(timer[0]));
    cellAction.appendChild(document.createTextNode(t9n.object.actions[timer[1]]));
    cellDows.appendChild(document.createTextNode(
      timer[2].map(dow => t9n.object.weekdays[dow]).join(', ')
    ));
    /* Decorate a remote platform timer's data */
    if (timer[4]) row.classList.add(timer[4]);
    /* Decorate a modified row */
    if (timer[5]) row.classList.add(timer[5]);

    /* No controls for unauthenticated users */
    if (!authenticated) return;

    /* Add Edit and Delete buttonss */
    addCommandCell(row, 1, !timer[4]); // Icon Edit
    addCommandCell(row, 2, !timer[4]); // Icon Delete
  }
}
/*
 * Clear a schedule table all but caption and headers
 */
async function clearSchedule() {
  while(table.rows.length > 1) {
    table.deleteRow(1);
  }
}
/*
 * Creat a new empty timer's form
 */
async function openEditForm(command) {
  const listenerOptions =  passiveSupported ? { passive: true } : false;
  const hRule = document.createElement('hr');
  const headerForm = document.createElement('h2');
  const buttonX = document.createElement('span');
  const buttonOk = document.createElement('button');
  const buttonCancel = document.createElement('button');
  const tableForm = document.createElement('table');
  /* Finish with an old form if there is one */
  closeEditForm();
  /* Begin with a form for a new row */
  table.rows[rowIndex + 1].dataset.current = true;

  buttonX.innerHTML = '&times;';
  buttonX.classList.add('modal-close');
  buttonX.id = 'edit_close';

  buttonCancel.innerText = _('Cancel');
  buttonOk.innerText = _('OK');
  buttonOk.dataset.command = command;

  headerForm.innerText = t9n.object.commands[command] + ' #' + rowIndex;
  headerForm.id = 'header';

  tableForm.id = 'timer';
  /* Time row */
  const rowTime = tableForm.insertRow(-1);
  rowTime.insertCell(-1).innerText = _('Time');
  cellEditTime = rowTime.insertCell(-1);
  cellEditTime.id = 'time';
  /* Action row */
  const rowAction = tableForm.insertRow(-1);
  rowAction.insertCell(-1).innerText = _('Action');
  cellEditAction = rowAction.insertCell(-1);
  cellEditAction.id = 'action';
  /* Weekdays row */
  const rowDows = tableForm.insertRow(-1);
  rowDows.insertCell(-1).innerText = _('Weekdays');
  cellEditDows = rowDows.insertCell(-1);
  cellEditDows.id = 'weekdays';
  /* OK/Cancel buttons row */
  const buttons = document.createElement('center');
  buttons.append(buttonOk, ' ', buttonCancel);
  /* Draw the table */
  formEdit.append(buttonX, headerForm, hRule, tableForm, buttons);
  /* Make the form draggable by its header */
  if (authenticated) setMovable(formEdit, headerForm);
  /* Button's handlers */
  buttonOk.addEventListener('click', handleOkButton, listenerOptions);
  buttonCancel.addEventListener('click', closeEditForm, listenerOptions);
  buttonX.addEventListener('click', closeEditForm, listenerOptions);
}
/*
 * Fills the empty timer's form with values
 */
async function populateEditForm(timer, enabled = true) {
  /* Time field */
  const timeBox = document.createElement('input');
  timeBox.disabled = !enabled;
  timeBox.type = 'time';
  timeBox.value = timer[0];
  cellEditTime.appendChild(timeBox);
  /* Switch state field */
  const selectList = document.createElement('select');
  selectList.disabled = !enabled;
  for (const [action, description] of Object.entries(t9n.object.actions)) {
    const option = document.createElement('option');
    if (action === timer[1]) {
      option.selected = 'selected';
    }
    option.value = action;
    option.appendChild(document.createTextNode(description));
    selectList.appendChild(option);
  }
  cellEditAction.appendChild(selectList);
  /* Set of weekdays checkboxes */
  for (let i = 1; i <= 7; i++) {
    const label = document.createElement('label');
    const checkBox = document.createElement('input');
    const brElement = document.createElement('br');
    checkBox.disabled = !enabled;
    checkBox.type = 'checkbox';
    checkBox.value = i;
    /* Set marks for each days of the week */
    if (timer[2].indexOf(i) > -1) {
      /* From 1st to 7th days of the week */
      checkBox.checked = true;
    } else if ((i == 7) && (timer[2].indexOf(0) > -1)) {
      /* The 0th day is treated in the same way as the 7th, as Sunday */
      checkBox.checked = true;
    }
    label.append(t9n.object.weekdays[i], brElement, checkBox);
    cellEditDows.appendChild(label);
  }
}
/*
 * Closes the form. Also used as a handler for onClick event
 */
async function closeEditForm() {
  delete table.rows[rowIndex + 1].dataset.current;
  formEdit.style.display = 'none';
  while (formEdit.lastChild) {
    formEdit.removeChild(formEdit.lastChild);
  }
  /* "Just to do it right" */
  cellEditTime = cellEditAction = cellEditDows = null;
}
/*
 * Save the schedule
 */
async function handleSaveButton() {
  //let error = 'timeout';
  let scheduleToSave = {};
  const preparedToSave = timers.filter(timer => !timer[4]).sort();
  scheduleToSave[relay] = {'timers': preparedToSave };

  const saveRequest = new Request(saveUrl, {
    method: 'POST',
    cache: 'no-cache',
    headers: new Headers(),
    body: JSON.stringify(scheduleToSave)
  });

  const data = await fetchJson(saveRequest);
  if (data.message !== undefined) alert(data.message);
  if (data.error === 'OK') redirectToMain();
}
/*
 * Sorting function for schedule
 */
function compareFirstColumn(a, b) {
  const str1 = (a[3] + a[0]).toString();
  const str2 = (b[3] + b[0]).toString();

  return str1.length - str2.length || str1.localeCompare(str2);
}
/*
 * 'OK' button of schedule edit|add|delete form handler
 */
async function handleOkButton(event) {
  const command = Number(event.target.dataset.command);

  switch (command) {
    case 0:
    case 1:
      const time = cellEditTime.children[0].value;
      const action = cellEditAction.children[0].value;
      let dows = [];
      for (let cell of cellEditDows.children) {
        const chkBox = cell.children[1];
        if (chkBox.checked) {
          dows.push(parseInt(chkBox.value));
        }
      }
      /* Three regular members and class for new record */
      timers[rowIndex] = [time, action, dows, 0, '', 'notsaved'];
      populateSchedule();
      break;
    case 2:
      /* Delete a timer from array of timers and so do with the table row */
      timers.splice(rowIndex, 1);
      table.deleteRow(rowIndex + 1); // 1 row for <th>
      break;
  }
  /* Clear and close the form */
  closeEditForm();
}
/*
 * Shows one of schedule edit|add|delete forms
 */
async function handleEditForm(event) {
  const element = event.target;
  const command = Number(element.dataset.command);
  rowIndex = element.parentNode.rowIndex - 1; // 1 row for <th>

  let timer = timers[rowIndex];
  let enabled = false;
  /* 0:add, 1:edit, 2:delete */
  switch (command) {
    case 0:
      timer = [new Date().toLocaleTimeString([], {timeStyle: "short"}),0,[1,2,3,4,5,6,7]];
      /* Fall-trough */
    case 1:
      enabled = true;
      /* Fall-trough */
    case 2:
      openEditForm(command);
      populateEditForm(timer, enabled);
      break;
    default:
      return;
  }
  formEdit.style.display = 'block';
}

const l10ns = document.querySelectorAll('[data-l10n]');
let offset = [0, 0];
let isMoving = false;

/* Load an object translation dictionary */
await t9n.load('/js/lang/');

/* Load a phrases dictionary */
await dict.load('/languages/lang_');
await dict.load('/languages/user/lang_');

createSchedule();
populateSchedule();

/* Localize the elements */
for (const elem of l10ns) {
  const desc = elem.innerText;
  elem.innerText = _(desc);
}
