let passiveSupported = false;

const status = (result) => {
  if (result.ok) {
    return result.json();
  }
  return Promise.reject(result);
}
//const json = (response) => response.json();
const fetchJson = (request) => fetch(request)
  .then(status)
//  .then(json)
  .catch(error => {
    console.log(error.status, error.statusText);
    return Promise.resolve({});
  });

/* Make the container collapsible */
function collapsible(element, anchor) {
  const cookies = getCookies();
  anchor.addEventListener('click', setContentVisible);

  /* Collapse the section if the cookies say that */
  if (cookies[element.id] == 'false') {
    setContentVisible({ target: anchor });
  }

  /* Anchor click event handler */
  function setContentVisible(event) {
    const anchor = event.target;
    const element = anchor.parentElement;
    const content = element.querySelector('div');
    const action = (content.style.display == 'none');

    setCookie(element.id, action, true);
    if (action) {
      element.classList.remove('collapsed');
      content.style.display = null;
    } else {
      element.classList.add('collapsed');
      content.style.display = 'none';
    }
  }
}

/* Make the element movable */
function setMovable(element, anchor) {
  const listenerOptions = passiveSupported ? { passive: true, capture: true } : true;

  anchor.addEventListener('mousedown',
    function(e) {
      isMoving = true;
      offset = [
        element.offsetLeft - e.clientX,
        element.offsetTop - e.clientY
      ];
      element.addEventListener('mouseup', elementStopMove, listenerOptions);
      window.addEventListener('mousemove', elementDoMove, listenerOptions);
      e.stopPropagation();
      e.preventDefault();
    }, true
  );

  function elementDoMove(e) {
    if (isMoving) {
      element.style.top = (e.clientY + offset[1]) + 'px';
      element.style.left = (e.clientX + offset[0]) + 'px';
      e.stopPropagation();
    }
  }

  function elementStopMove(e) {
    if (isMoving) {
      isMoving = false;
      window.removeEventListener('mousemove', elementDoMove);
      e.target.removeEventListener('mouseup', elementStopMove);
      e.stopPropagation();
    }
  }
}

/* Redirect to the main view in the end */
function redirectToMain() {
  window.location.href = '/';
}

/* Get a dictionary of cookies */
function getCookies() {
  return document.cookie.split('; ').reduce((acc, item) => {
    const [name, value] = item.split('=');
    return { ...acc, [name]: value };
  }, {});
}

/* Set a cookie */
function setCookie(name, value, persistent=false) {
  let cookie = name + '=' + value + ';secure;samesite=strict';
  if (persistent)
    cookie += ';max-age=604800';
  document.cookie = cookie;
}

try {
  const options = {
    get passive() {
      passiveSupported = true;
      return false;
    },
  };

  window.addEventListener("test", null, options);
  window.removeEventListener("test", null, options);
} catch (err) {
  passiveSupported = false;
}
