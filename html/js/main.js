import PeriodicRequest from '/js/PeriodicRequest.js';

const switchboard = document.querySelectorAll('.switchboard')[0];
const relaySwitches = switchboard.querySelectorAll('.state label.switch input[type="checkbox"]');
const timeCharts = switchboard.querySelectorAll('.schedule');
const l10ns = document.querySelectorAll('[data-l10n]');

let offset = [0, 0];
let isMoving = false;
let dummyRelays = {'status': {}, 'byhand': {}};

await dict.load('/languages/lang_');
await dict.load('/languages/user/lang_');

/* Open relay timers configuration page */
const relayConfig = (event) => {
  const outlet = event.target.parentElement.parentElement.dataset.relayName;
  window.location.href = '/edit.php?outlet=' + outlet;
};

/* Visualize the switch status by setting the "src" parameter of its visualisation
   image (modules "aqua", "aqua-z"...) to the right file for its status.
   It could be different for different visialisation modules in future, but
   it is the same by now.
 */
const visuals = (switchName, switchStatus) => {
  const imageElem = document.getElementById('image_' + switchName);
  if (!imageElem) return;

  /* Avoid to reload an image for each request when right image is already loaded.
     .src returns already resolved url, like 'scheme://hostname/path', even if relative url was assigned.
     .getAttribute('src') returns exactly last assigned value (relative or absolute url).
   */
  const imageUrl = images[switchName][switchStatus];
  if (imageElem.getAttribute('src') !== imageUrl) {
    imageElem.src = imageUrl;
  }
}

/* Set the button to right state */
const switches = (switchName, switchStatus) => {
  const elem = document.getElementById(switchName);
  if (elem && (elem.type === 'checkbox')) {
    elem.checked = (switchStatus === 'ON');
  }
}

/* Periodically emulates relays status for demo mode */
function getDummySwitches(platform) {
  return checkDummy(dummyRelays);
}

/* Periodically actualize relays status */
const actualizeSwitches = (data) => {
  for(const [switchName, switchStatus] of Object.entries(data)) {
    /* Skip the record if data is not correct */
    if (switchStatus === undefined) continue;

    /* Set the button to right state */
    switches(switchName, switchStatus);
    /* Set the right image for the action */
    visuals(switchName, switchStatus);
  }
}

/* Get a list of actual states of dummy relays and
   actialize it for current time
 */
function checkDummy(data) {
  const cur_timestamp = (new Date()).getTime();
  /* Iterate outlets for their timers */
  for (const [name, timers] of Object.entries(outletsTimers)) {
    /* Collect the outlet timers with real timestamps for one week long */
    let week = [];
    for (const timer of timers) {
      for (let i=0; i<7; i++) {
        let i_time = new Date(cur_timestamp);
        i_time.setDate(i_time.getDate() - i);
        const i_dow = (i_time.getDay() > 0) ? i_time.getDay() : 7;
        /* The timer is for each weekday when no timer.weekday defined */
        if (timer[2] == undefined || timer[2].indexOf(i_dow) !== -1) {
          const time = timer[0].split(':');
          i_time.setHours(time[0], time[1], 0, 0);
          week.push({ timestamp: i_time.getTime(), action: timer[1], dow: i_dow });
        }
      }
    }
    let action = 'OFF';
    if (week.length > 0) {
      /* Order timers by time descending */
      week.sort(function(a, b) { return b.timestamp - a.timestamp; });
      action = week[0].action;
      for (const timer of week) {
        if (cur_timestamp > timer.timestamp) {
          action = timer.action;
          break;
        }
      }
    }
    /* Construct a response for all outlets statuses */
    const status = action.toUpperCase() === 'OFF' ? 'OFF' : 'ON';
    if (data.status[name] == undefined) {
      data.status[name] = status;
    } else if (data.byhand[name]) {
      if (data.status[name] === status) delete data.byhand[name];
    } else {
      data.status[name] = status;
    }
  }

  return data.status;
}

/* Switch the fake relay */
const switchDummy = (event) => {
  const elem = event.target;
  if (!elem.id) return;

  const requestOutlets = PeriodicRequest.instances.outlets;
  /* Change the status to an opposite value */
  const status = dummyRelays.status[elem.id].toUpperCase() === 'ON' ? 'OFF' : 'ON';
  dummyRelays.status[elem.id] = status;
  dummyRelays.byhand[elem.id] = true;

  const data = checkDummy(dummyRelays);
  actualizeSwitches(data);
};

/* Prepare and send a request for change the state of the relay.
   A list of actual states of relays will be received as result.
 */
const switchRelay = async (event) => {
  const elem = event.target;
  if (!elem.id) return;

  const requestOutlets = PeriodicRequest.instances.outlets;
  const relayToChange = {n: elem.id, t: (elem.checked) ? 'on' : 'off' };

  const data = await fetchJson(new Request(
    requestOutlets.template,
    { body: JSON.stringify(relayToChange) }
  ));
  for (const callback of requestOutlets.handlers) {
    callback(data);
  }
}

function handleSwitchboxes() {
  const handler = handlers['switch'];
  for (const elem of relaySwitches) {
    elem.addEventListener(
      'click', handler, passiveSupported ? { passive: true } : false
    );
  }
}

function handleSchedules() {
  const handler = handlers['schedule'];
  for (const chart of timeCharts) {
    const anchor = document.createElement('span');
    anchor.addEventListener(
      'click', handler, passiveSupported ? { passive: true } : false
    );
    anchor.classList.add('anchor');
    chart.classList.add('configurable');
    chart.appendChild(anchor);
  }
}

/* Localize the elements */
for (const elem of l10ns) {
  const desc = elem.innerText;
  elem.innerText = _(desc);
}

/* Periodically renew a list of states of relays, real or emulated */
let handlers = {'schedule': relayConfig, 'switch': switchRelay};
if (demoMode) {
  const emulateOutlets = PeriodicRequest.registerTaskType('outlets', getDummySwitches);
  emulateOutlets.addHandler(actualizeSwitches);
  emulateOutlets.registerPlatform('outlets');
  /* Select the handlers */
  handlers.switch = switchDummy;
} else {
  const requestOutlets = PeriodicRequest.registerRequestType('outlets', '/requests/relay.php');
  requestOutlets.addHandler(actualizeSwitches);
  /* Run requests to all platforms */
  for (const name of relayPlatforms) {
    requestOutlets.registerPlatform(name, {n: name, t: 'status'});
  }
  /* Select the handlers */
  if (!authenticated) {
    handlers.switch = authOn;
  }
}

/* Handle all collapsible sections */
const collapsibleSections = document.querySelectorAll('section.collapsible');
for (const section of collapsibleSections) {
  const anchor = section.querySelector('h2');
  collapsible(section, anchor);
}

/* Install selected handlers */
handleSwitchboxes();
handleSchedules();

/* Run all registered periodic tasks (incl regitered by plugins) */
PeriodicRequest.startAll();
