<?php  # $Id: lang_en.inc.php,v 1.1 2019/01/04 11:37 mishin Exp $
@define('ERROR_CANNOT_OPEN_FILE',	"Can't open file '%s'");
@define('ERROR_CANNOT_DECODE_JSON',	"Something wrong with '%s'");
@define('ERROR_NOT_FOUND',		"Object '%s' not found");
@define('ERROR_PLATFORM_NOT_FOUND',	"Platform '%s' not found");
@define('ERROR_PLATFORM_TYPE_WRONG',	"Unknown platform type '%s'");
@define('ERROR_EXTENSION_LOADED',	"PHP extension '%s' is not loaded.");
@define('ERROR_SCHEDULER_MAPS_OUTDATED',"The task sheduler maps may be out of date.");
@define('ERROR_SCHEDULER_MAPS_WRITE',	"Image '%s' write is failed.");

@define('STATUS_OFF',			'Off');
@define('STATUS_ON',			'On');

@define('AUTH_LOGIN',			'Login');
@define('AUTH_LOGOUT',			'Logout');
@define('USERNAME',			'Username');
@define('PASSWORD',			'Password');

@define('AQUA1_DESC',			'Aquarium');
@define('AQUA2_DESC',			'Nano aquarium');

@define('TIME',				'Time');
@define('ACTION',			'Action');
@define('WEEKDAYS',			'Weekdays');

@define('ADD',				'Add');
@define('EDIT',				'Edit');
@define('DELETE',			'Delete');

@define('OK',				'OK');
@define('SAVE',				'Save');
@define('CANCEL',			'Cancel');

@define('DEMO_MODE',			'Demo<br>mode');
@define('PROJECT_ON_GITLAB',		'Project<br>on GitLab');
?>
