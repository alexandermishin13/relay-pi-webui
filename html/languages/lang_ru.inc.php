<?php  # $Id: lang_ru.inc.php,v 1.1 2019/01/04 12:15 mishin Exp $

setlocale(LC_ALL, 'ru_RU.UTF-8');

@define('ERROR_CANNOT_OPEN_FILE',	"Не удалось открыть файл '%s'");
@define('ERROR_CANNOT_DECODE_JSON',	"Что-то не так с '%s'");
@define('ERROR_NOT_FOUND',		"Объект '%s' не найден");
@define('ERROR_PLATFORM_NOT_FOUND',	"Платформа '%s' не найдена");
@define('ERROR_PLATFORM_TYPE_WRONG',	"Неизвестный тип платформы '%s'");
@define('ERROR_EXTENSION_LOADED',	"PHP модуль '%s' не загружен.");
@define('ERROR_SCHEDULER_MAPS_OUTDATED',"Изображения планировщика для реле могут быть неактуальными.");
@define('ERROR_SCHEDULER_MAPS_WRITE',	"Изображение '%s' не может быть записано.");

@define('STATUS_OFF',			'Выкл');
@define('STATUS_ON',			'Вкл');

@define('AUTH_LOGIN',			'Войти');
@define('AUTH_LOGOUT',			'Выйти');
@define('USERNAME',			'Имя');
@define('PASSWORD',			'Пароль');

@define('AQUA1_DESC',			'Аквариум');
@define('AQUA2_DESC',			'Нано-аквариум');

@define('TIME',				'Время');
@define('ACTION',			'Действие');
@define('WEEKDAYS',			'Дни недели');

@define('ADD',				'Добавить');
@define('EDIT',				'Изменить');
@define('DELETE',			'Удалить');

@define('OK',				'OK');
@define('SAVE',				'Сохранить');
@define('CANCEL',			'Отмена');

@define('DEMO_MODE',			'Демонстрационный<br>режим');
@define('PROJECT_ON_GITLAB',		'Проект<br>на GitLab');
?>
