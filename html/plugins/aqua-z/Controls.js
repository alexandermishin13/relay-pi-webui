class Controls {
  static controller = null;
  constructor(container) {
    let pIndex = 0;
    const p = [
      document.createElement('p'),
      document.createElement('p')
    ];
    /* Position and dimensions widgets */
    for (const [descr, inputs] of Object.entries({
      'Position': ['left', 'top'],
      'Dimensions': ['width', 'height']
    })) {
      const fieldset = document.createElement('fieldset');
      const legend = document.createElement('legend');
      for (const parameter of inputs) {
        const control = document.createElement('input');
        control.type = 'number';
        control.dataset.name = parameter;
        fieldset.appendChild(control);
        this[parameter] = control;
      }
      legend.innerText = descr;
      legend.dataset.l10n = '';
      fieldset.append(legend);
      p[pIndex].appendChild(fieldset);
      pIndex = Math.abs(pIndex - 1);
    }
    this.left.insertAdjacentText('afterend', ':');
    this.width.insertAdjacentText('afterend', ':');
    /* Decoration mirror widget */
    const fieldset = document.createElement('fieldset');
    const legend = document.createElement('legend');
    for(const [descr, parameter] of Object.entries({
      'X-axis': 'flipX',
      'Y-axis': 'flipY'
    })) {
      const label = document.createElement('label');
      const control = document.createElement('input');
      label.className = 'switch';
      label.htmlFor = parameter;
      label.innerText = descr;
      control.type = 'checkbox';
      control.dataset.name = parameter;
      control.id = parameter;
      this[parameter] = control;
      label.appendChild(control);
      fieldset.append(label, ' ');
    }
    legend.innerText = 'Mirror';
    legend.dataset.l10n = '';
    fieldset.append(legend);
    p[pIndex].appendChild(fieldset);
    /* Draw the paragraphs with inactive widgets */
    container.append(...p);
    this.disable();
  }
  /*
   * Enable access to the controls
   */
  enable(instance) {
    /* Create a new abort controller for all the instance events listeners */
    this.constructor.controller = new AbortController;
    for (const property of Object.keys(this)) {
      this[property].disabled = false;
      this[property].addEventListener('change', e => instance.syncFromControls(e), { signal: this.constructor.controller.signal });
      if (this[property].type === 'checkbox') {
        this[property].checked = instance[property] ? '1' : '';
        this[property].labels[0].classList.remove('disabled');
      } else {
        this[property].value = instance[property];
      }
    }
    this.width.max = instance.maxWidth;
    this.width.min = instance.minWidth;
    this.height.max = instance.maxHeight;
    this.height.min = instance.minHeight;
  }
  /*
   * Disable access to the controls
   */
  disable(instance = null) {
    /* Abort all the instance events listeners when the instance disabled */
    if (!Object.is(instance, null)) {
      this.constructor.controller.abort();
    }
    /* There is no harm in forcibly disabling controls */
    for (const property of Object.keys(this)) {
      this[property].disabled = true;
      if (this[property].type === 'checkbox') {
        this[property].checked = '';
        this[property].labels[0].classList.add('disabled');
      } else {
        this[property].value = '';
      }
    }
  }
  /*
   * Update controls values from a now selected decoration
   */
  update(instance) {
    if (Object.is(instance, null)) return;

    for (const property of Object.keys(this)) {
      this[property].value = instance[property];
    }
  }
}
