import Random from './Random.js';
//import {Scenery} from './Scenery.js';

if (!String.prototype.format) {
  String.prototype.format = function () {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
      return typeof args[number] != "undefined" ? args[number] : match;
    });
  };
}

export class Creature {
  static delay = 0;
  static derived = {};
  static #seed = new Random(); // a relatively fast random generator
  creature = {};
  constructor(specimenArea, scenery, delay = 500) {
    this.id = specimenArea.id;
    this.specimenArea = specimenArea;
    this.constructor.instances[this.id] = this;
    this.constructor.delay -= delay + Creature.#seed.value * 1000;
    this.scenery = scenery;
    this.specimen = specimenArea.children[0];
    this.specimen.style.display = 'initial';
    /* Collect scenery dimensions */
    this.areaStyle = getComputedStyle(specimenArea);
    this.area = {
      left: parseFloat(this.areaStyle.left),
      top: parseFloat(this.areaStyle.top),
      width: parseFloat(this.areaStyle.width),
      height: parseFloat(this.areaStyle.height),
    };
    this.area.freeze;
    /* Get easing function from a css definition */
    this.easing = this.areaStyle.animationTimingFunction;
    /* Set parameters for the whole iteration, not for individual keyframes */
    this.keyframeEffect = new KeyframeEffect(this.specimen, null, {
      easing: 'linear',
      fill: 'forwards', // helps a lot
      iterations: 1,
      duration: this.constructor.duration * Creature.#seed.fractionLess(0.1),
    });
    this.animation = new Animation(this.keyframeEffect);
    this.animation.startTime = this.constructor.delay;
    /* An arrow function is used for handler as "this" is not "animation" */
    this.animation.addEventListener('finish', (e) => this.breathOfLife(e));
    /* Start the animation */
    this.setAnimation(this.area);
  }
  /**
   * Starts every time the fish animation ends to run it again
   */
  async breathOfLife(e) {
    e.preventDefault;
    const prevArea = {
      left: parseFloat(this.areaStyle.left),
      top: parseFloat(this.areaStyle.top),
      width: parseFloat(this.areaStyle.width),
      height: parseFloat(this.areaStyle.height),
    };
    /* Restart the animation */
    this.setAnimation(prevArea).then(this.animation.play());
  }
  /**
   * Compute an animation properties
   */
  async setAnimation(prevArea) {
    const areaTop = this.constructor.surface ? 0 : this.area.height * Creature.#seed.fraction(0.2);
    const areaLeft = this.area.width * Creature.#seed.fraction(0.2);

    const top = this.area.top + areaTop;
    const left = this.area.left + areaLeft;
    const height = this.area.height * Creature.#seed.fractionLess(0.4) - areaTop;
    const width = this.area.width * Creature.#seed.fractionLess(0.4) - areaLeft;
    /* Get a random animation. Haven't even tried this yet */
    const index = Math.floor(Creature.#seed.value * this.constructor.keyframes.length);
    const keyframes = Object.assign({easing: this.easing}, this.constructor.keyframes[index]);
    /* Adapt the starting frames of the next animation to the creature's final position */
    const style = getComputedStyle(this.specimen);
    const matrix = new DOMMatrixReadOnly(style.transform)
    const newOffsetTop = prevArea.top - top + matrix.m42;
    const newOffsetLeft = prevArea.left - left + matrix.m41;
    for (const i in this.constructor.keyframesStart) {
      keyframes.transform[i] = this.constructor.keyframesStart[i].format(newOffsetLeft, newOffsetTop);
    }
    /* Renew keyframe effects */
    this.keyframeEffect.duration = this.constructor.duration * Creature.#seed.fractionLess(0.1);
    this.keyframeEffect.setKeyframes(keyframes);

    const realArea = this.specimenArea.style;
    realArea.top = top + 'px';
    realArea.left = left + 'px';
    realArea.height = height + 'px';
    realArea.width = width + 'px';
  }
}

export class Gourami extends Creature {
  static { super.derived[this.name.toLowerCase()] = this; };
  static surface = true; // fixed surface for gulp of air
  static duration = 45000; //ms
  static instances = {};
  static keyframes = [
    {
      transform: ['translate(130px,0) rotateZ(-27deg)','translate(80cqw,68cqh) rotateZ(15deg)','translate(100cqw,42cqh)','translate(97cqw,42cqh) rotateY(180deg)','translate(80cqw,68cqh) rotateY(180deg) rotateZ(-5deg)','translate(60cqw,20cqh) rotateY(180deg)','translate(40cqw,82cqh) rotateY(180deg) rotateZ(22deg)','translate(20cqw,50cqh) rotateY(180deg) rotateZ(-5deg)','translate(0,70cqh) rotateY(180deg)','translate(3cqw,70cqh)','translate(20cqw,50cqh)','translate(40cqw,82cqh)','translate(130px,0) rotateZ(-27deg)'],
      offset: [0,.1,.2,.22,.3,.4,.5,.6,.7,.72,.8,.9],
    },
  ];
  static keyframesStart = ['translate({0}px,{1}px) rotateZ(-27deg)'];
  constructor(...args) {
    super(...args);
  }
}

export class School extends Creature {
  static { super.derived[this.name.toLowerCase()] = this; };
  static surface = false;
  static duration = 15000; //ms
  static instances = {};
  static keyframes = [
    {
      transform: ['translate(2cqw,15cqh)','translate(20cqw,27cqh) rotateZ(5deg)','translate(40cqw,70cqh) rotateZ(10deg)','translate(50cqw,65cqh)','translate(60cqw,60cqh)','translate(80cqw,48cqh)','translate(100cqw,24cqh)','translate(98cqw,24cqh) rotateY(180deg)','translate(80cqw,0cqh) rotateY(180deg)','translate(60cqw,36cqh) rotateY(180deg)','translate(40cqw,27cqh) rotateY(180deg) rotateZ(0deg)','translate(20cqw,42cqh) rotateY(180deg) rotateZ(-10deg)','translate(0,15cqh) rotateY(180deg)','translate(0,15cqh)'],
      offset: [0,.1,.2,.3,.35,.4,.5,.52,.6,.7,.8,.9,.98],
    },
  ];
  static keyframesStart = ['translate({0}px,{1}px)'];
  constructor(...args) {
    super(...args);
  }
}
