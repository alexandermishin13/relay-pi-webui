import {Scenery} from './Scenery.js';

export class Decoration {
  static selected = null;
  static clientRectEdge = 4;
  static #mousePosition = -1;
  static #initial = {};
  /***
   * The order of values is important!!! for #getMouseArea() algorithm
   ***/
  static #cursors = [
    'w-resize', 'e-resize',
    'n-resize', 'nw-resize', 'ne-resize',
    's-resize', 'sw-resize', 'se-resize',
    'move', 'default'
  ];
  scale = undefined;
  constructor(id, sceneryElement) {
    this.id = id;
    this.sceneryElement = sceneryElement;
    this.decoElement = document.getElementById(id);

    const style = window.getComputedStyle(this.decoElement);
    this.#normalizeImageStyle(style);
    this.left = parseInt(style.left);
    this.top = parseInt(style.top);
    this.width = parseInt(style.width);
    this.height = parseInt(style.height);
    this.maxWidth = this.decoElement.naturalWidth;
    this.maxHeight = this.decoElement.naturalHeight;
    this.minWidth = this.minHeight = this.constructor.clientRectEdge * 2;
  }
  /*
   * Create a label for decoration
   */
  createLabel() {
    this.label = new Label(this);
  };
  /*
   * Create controls for current decoration
   */
  static createControls(container) {
    if (Object.is(Label.controls, null))
      Label.controls = new Controls(container);
  }
  /*
   * Normalize the values before deal with them
   */
  #normalizeImageStyle(style) {
    /* Get scale values for the instance image */
    let [scaleX, scaleY] = style.scale.trim().split(' ').map(n => {
      if (typeof n !== 'undefined') {
        const matches = n.match(/^(\-?\d+\.?\d*)(%)?$/);
        if (matches !== null) {
          return (matches[2]) ? matches[1]/100 : +matches[1];
        }
      }
      return undefined;
    });
    const [originX, originY] = style.transformOrigin.split(' ').map(n => parseInt(n));
    const top = parseInt(style.top);
    const left = parseInt(style.left);
    const width = parseInt(style.width);
    const height = parseInt(style.height);

    if (scaleX !== undefined) {
      /* If there is the only scale for x then clone it for y too */
      if (scaleY === undefined) scaleY = scaleX;
      /* Minimize the effect of the "scaling" (read "transform") option whenever possible */
      const [signScaleX, signScaleY] = [Math.sign(scaleX), Math.sign(scaleY)];
      if (signScaleX !== signScaleY) {
        this.scale = signScaleX + ' ' + signScaleY;
        this.decoElement.style.scale = this.scale;
      } else if (signScaleX != 1) {
        this.scale = signScaleX;
        this.decoElement.style.scale = this.scale;
      } else {
        this.scale = undefined;
        this.decoElement.style.removeProperty('scale');
      }
    } else {
      this.scale = undefined;
      scaleX = 1;
      scaleY = 1;
    } // if (k.length > 0)

    this.flipX = (scaleX < 0);
    this.flipY = (scaleY < 0);

    this.decoElement.style.width = parseInt(width * Math.abs(scaleX)) + 'px';
    this.decoElement.style.height = parseInt(height * Math.abs(scaleY)) + 'px';
    this.decoElement.style.left = parseInt(left + originX * (1 - Math.abs(scaleX))) + 'px';
    this.decoElement.style.top = parseInt(top + originY * (1 - Math.abs(scaleY))) +'px';
  }
  /*
   * Copy decoration parameters from the controls
   */
  syncFromControls(e) {
    const name = e.target.dataset.name;
    switch(name) {
      case 'width':
        /* style.width will be set to correct value */
        this.#changeImageWidth(e.target.value);
        e.target.value = this.width;
        break;
      case 'height':
        /* style.height will be set to correct value */
        this.#changeImageHeight(e.target.value);
        e.target.value = this.height;
        break;
      case 'flipX':
      case 'flipY':
        const checked = e.target.checked;
        this[name] = checked;
        this.label[name] = checked;
        /* Turn flipX or flipY switch values into a .style.scale value */
        const scale = {
          'flipX': this.flipX ? -1 : 1,
          'flipY': this.flipY ? -1 : 1,
        };
        if (scale.flipY != scale.flipX) {
          this.scale = scale.flipX + ' ' + scale.flipY;
          this.decoElement.style.scale = this.scale;
        } else if (scale.flipX < 1) {
          this.scale = '' + scale.flipX;
          this.decoElement.style.scale = this.scale;
        } else {
          this.scale = undefined;
          this.decoElement.style.removeProperty('scale');
        }
        /* update label and controls */
        this.label.updateData();
        break;
      default:
        this.decoElement.style[name] = e.target.value + 'px';
        this[name] = Number(e.target.value);
    }
  }
  /*
   * Select an action on the instance's dimention
   */
  static #selectMouseAction(eventDeco) {
    if (Object.is(Decoration.selected, null)) return;

    eventDeco.preventDefault();
    const dir = Decoration.selected.#getMouseArea(eventDeco);
    Decoration.selected.sceneryElement.style.cursor = Decoration.#cursors[dir];
  }
  /*
   * Mouse event handler for element modifications
   */
  static #modifyElement(e) {
    const pos = Decoration.#mousePosition;
    /* Do nothing when pointer is out of bounds or
       there is no selected decoration at all */
    if (pos > 8 && pos < 0) return;

    e.preventDefault();
    const instance = Decoration.selected;
    const ev = (e.type === "touchmove") ? e.touches[0] : e;

    /* Is this a move, or resize */
    if (pos == 8) {
      instance.#mouseDragElement(ev);
      /* update label and controls */
      instance.label.updateData();
      Label.controls.update(Decoration.selected);
    } else {
      instance.#mousePullEdge(ev);
      /* autoupdate label and controls */
    }
  }

  static scaleElement(e) {
    e.preventDefault();
    Decoration.selected.wheelScale(e);
  }
  /*  🡬 🡩 🡭
   *  🡨 ⮽ 🡪
   *  🡯 🡫 🡮 inside, borders and outside
   */
  #getMouseArea(ev) {
    const edge = this.constructor.clientRectEdge;
    const dim = this.decoElement.getBoundingClientRect();

    /* Check if the coursor is out of bounds */
    if ((ev.clientY < dim.top - edge) ||
        (ev.clientX < dim.left - edge) ||
        (ev.clientY > (dim.bottom + edge)) ||
        (ev.clientX > (dim.right + edge))
    ) return 9;

    let result = -1;
    /* Check the instance's edges and corners.
       Left and right first... */
    if (Math.abs(ev.clientX - dim.left) < edge)
      result = 0;
    else
    if (Math.abs(ev.clientX - dim.right) < edge)
      result = 1;
    /* ...then top and bottom */
    if (Math.abs(ev.clientY - dim.top) < edge)
      result += 3;
    else
    if (Math.abs(ev.clientY - dim.bottom) < edge)
      result += 6;
    /* If result stays untoched then the cursor is inside the bounds */
    if (result < 0) result = 8;

    return result;
  }
  /*
   * Set the selected instance pointer and mark its visuals
   * Start to monitor the image style changes and mousedown events
   */
  select() {
    /* Mark DOM objects as selected */
    this.decoElement.style.animation = 'none';
    this.decoElement.classList.add('selected');
    /* Set the pointer for the selected instance */
    this.constructor.selected = this;
    /* Bind event handlers on the selected scene */
    const scenery = this.sceneryElement;
    scenery.addEventListener('mousemove', this.constructor.#selectMouseAction);
    scenery.addEventListener('mousedown', this.constructor.#lockPointer);
//    tank.addEventListener('wheel', this.constructor.scaleElement);
  }
  /*
   * Stop to monitor the image style changes and mousedown events.
   * Clear the selected instance pointer and unmark its visuals
   */
  unselect() {
    /* Release event handlers from the scene */
    const scenery = this.sceneryElement;
    scenery.removeEventListener('mousedown', this.constructor.#lockPointer);
    scenery.removeEventListener('mousemove', this.constructor.#selectMouseAction);
//    tank.removeEventListener('wheel', this.constructor.scaleElement);
    this.decoElement.classList.remove('selected');
    /* Clear the selected object pointer */
    this.constructor.selected = null;
    this.decoElement.style.animation = '';
  }
  /* Lock a selected element by pressing the left mouse button */
  static #lockPointer(e) {
    e.preventDefault(); // don't touch it
    const instance = Decoration.selected;
    const ev = (e.type === "touchstart") ? e.touches[0] : e;
    instance.left = instance.decoElement.offsetLeft;
    instance.top = instance.decoElement.offsetTop;

    Decoration.#initial = instance.decoElement.getBoundingClientRect();
    Decoration.#initial.offsetX = ev.clientX - instance.left;
    Decoration.#initial.offsetY = ev.clientY - instance.top;
    /* Get the mouse cursor position relative to instance image */
    Decoration.#mousePosition = instance.#getMouseArea(ev);
    /* Start to monitor mousemove and mouseup events */
    const scenery = instance.sceneryElement;
    scenery.removeEventListener('mousemove', Decoration.#selectMouseAction);
    scenery.addEventListener('mousemove', Decoration.#modifyElement);
    scenery.addEventListener('mouseup', Decoration.#unlockPointer);
//    tank.removeEventListener('wheel', Decoration.scaleElement);
  }
  /*
   * Release the element from locking
   */
  static #unlockPointer() {
    const instance = Decoration.selected;
    /* Stop to monitor mousemove and mouseup events */
    const scenery = instance.sceneryElement;
    scenery.removeEventListener('mousemove', Decoration.#modifyElement);
    scenery.removeEventListener('mouseup', Decoration.#unlockPointer);
    /* Clear the mouse cursor position */
    Decoration.#mousePosition = -1;
    Decoration.#initial = {};
    /* Return to cursor position monitoring */
    scenery.addEventListener('mousemove', Decoration.#selectMouseAction);
//    tank.addEventListener('wheel', Decoration.scaleElement);
  }
  /*
   * Set height of the decoElement to value between it's min and max
   */
  #changeImageHeight(height = this.height) {
    if (height < this.minHeight) height = this.minHeight;
    else
    if (height > this.maxHeight) height = this.maxHeight;

    this.decoElement.style.height = height + 'px';
    this.height = Number(height);
  }
  /*
   * Set width of the decoElement to value between it's min and max
   */
  #changeImageWidth(width = this.width) {
    if (width < this.minWidth) width = this.minWidth;
    else
    if (width > this.maxWidth) width = this.maxWidth;

    this.decoElement.style.width = width + 'px';
    this.width = Number(width);
  }
  /* Move an image of an instance by dragging */
  #mouseDragElement(ev) {
    this.top = parseInt(ev.clientY - this.constructor.#initial.offsetY);
    this.left = parseInt(ev.clientX - this.constructor.#initial.offsetX);
    this.decoElement.style.left = this.left + 'px';
    this.decoElement.style.top = this.top + 'px';
  }
  /* Resize an image of an instance by edge pulling */
  #mousePullEdge(ev) {
    const pos = this.constructor.#mousePosition;
    /* Check if LeftTop, CenterTop, and RigthTop corners
       then if LeftBottom, CenterBottom and RigthBottom
     */
    if ((pos === 2) || (pos === 3) || (pos === 4)) {
      const requestedHeight = this.constructor.#initial.bottom - ev.clientY;
      this.#changeImageHeight(requestedHeight);
      /* this.heigth has corrected value */
      this.top = parseInt(ev.clientY + requestedHeight - this.constructor.#initial.offsetY - this.height);
      this.decoElement.style.top = `${this.top}px`;
    } else if ((pos === 5) || (pos === 6) || (pos === 7)) {
      const requestedHeight = ev.clientY - this.constructor.#initial.top;
      this.#changeImageHeight(requestedHeight);
    }
    /* Also check if LeftMiddle, LeftTop and LeftBottom corners
       then if  RightMiddle, RightTop and RightBottom
     */
    if ((pos === 0) || (pos === 3) || (pos === 6)) {
      const requestedWidth = this.constructor.#initial.right - ev.clientX;
      this.#changeImageWidth(requestedWidth);
      /* this.width has corrected value */
      this.left = parseInt(ev.clientX + requestedWidth - this.constructor.#initial.offsetX - this.width);
      this.decoElement.style.left = `${this.left}px`;
    } else if ((pos === 1) || (pos === 4) || (pos === 7)) {
      const requestedWidth = ev.clientX - this.constructor.#initial.left;
      this.#changeImageWidth(requestedWidth);
    }
  }
  wheelScale(e) {
    const delta = (e.wheelDelta ? e.wheelDelta : -e.deltaY);
    let scaleX = this.maxWidth / this.width;
    let scaleY = this.maxHeight / this.height;
    let width = this.width;
    let height = this.height;

    if (delta > 0) {
      scaleX *= 1.05;
      scaleY *= 1.05;
    } else if (delta < 0) {
      scaleX /= 1.05;
      scaleY /= 1.05;
    }
    width *= scaleX;
    height *= scaleY;

    this.decoElement.style.height = `${this.height}px`;
    this.decoElement.style.width = `${this.width}px`;
  }
}
