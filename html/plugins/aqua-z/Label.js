class Label {
  static #labelNameWidth = 0;
  static #observer = new ResizeObserver(this.#updateDataHandler);
  static labels = document.querySelectorAll('.labels p')[0];
  static instances = {};
  static selected = null;
  static controls = null;
  constructor(instance) {
    /* Create a label */
    this.instance = instance;
    this.constructor.instances[instance.id] = instance;
    this.element = document.createElement('div');
    this.colMirrors = document.createElement('span');
    this.colMirrors.innerHTML = '<span>&nbsp;&mapstoleft;&nbsp;</span><span>&nbsp;&mapstodown;&nbsp;</span>';
    this.colMetrics = document.createElement('span');
    this.colMetrics.innerHTML = '<span>,</span><span>x</span>';
    this.colMetrics.className = 'metrics';
    this.element.appendChild(this.colMetrics);
    this.element.appendChild(this.colMirrors);
    this.element.classList.add('label');
    this.element.dataset.id = instance.id;
    /* Prepare an event handler for mouse clicks */
    this.element.addEventListener('click', e => this.#touchHandler(e));
    this.flipX = instance.flipX;
    this.flipY = instance.flipY;
    this.#observeDecoElement();
    this.updateData();
    /* Place the label */
    this.constructor.labels.insertAdjacentElement('beforeend', this.element);
    /* Calculate the label length */
    this.constructor.#labelNameWidth = Math.max(this.constructor.#labelNameWidth, instance.id.length);
    document.documentElement.style.setProperty('--label-name-width', this.constructor.#labelNameWidth + 'ex');
  }
  /*
   * Label click event handler
   */
  #touchHandler(e) {
    const selected = this.constructor.selected;
    /* Unselect selected instance and its label */
    if (!Object.is(selected, null)) {
      selected.element.classList.remove('selected');
      this.constructor.#observer.unobserve(selected.instance.decoElement);
      this.constructor.controls.disable(selected.instance);
      this.constructor.selected = null;
      selected.instance.unselect();
    }
    /* Select the instance if it was not selected one */
    if (!Object.is(selected, this)) {
      this.element.classList.add('selected');
      this.constructor.#observer.observe(this.instance.decoElement);
      this.constructor.controls.enable(this.instance);
      this.constructor.selected = this;
      this.instance.select();
    }
  }
  updateData() {
    if (Object.is(this.element, null)) return;

    const dataMetrics = this.colMetrics.children;
    /* Update position and dimensions */
    dataMetrics[0].setAttribute("data-before", this.instance.left);
    dataMetrics[0].setAttribute("data-after", this.instance.top);
    dataMetrics[1].setAttribute("data-before", this.instance.width);
    dataMetrics[1].setAttribute("data-after", this.instance.height);
    /* Update mirror data */
    const flip = [this.flipX, this.flipY];
    for (const i in flip) {
      if (flip[i]) {
        this.colMirrors.children[i].classList.add('enabled');
      } else {
        this.colMirrors.children[i].classList.remove('enabled');
      }
    }
  }
  #observeDecoElement() {
    const style = window.getComputedStyle(this.instance.decoElement);
    this.instance.left = parseInt(style.left);
    this.instance.top = parseInt(style.top);
    this.instance.width = parseInt(style.width);
    this.instance.height = parseInt(style.height);
  }
  /*
   * ResizeObserver's handler
   */
  static #updateDataHandler(entries) {
    for(const entry of entries) {
      const instance = Label.instances[entry.target.id];

      if (Object.is(instance.label.element, null)) continue;

      instance.label.#observeDecoElement();
      instance.label.updateData();
    }
    Label.controls.update(Label.selected.instance);
  }
}
