export default class Random {
  #buffer = '';
  #index = 0;
  constructor() {
    this.#generate();
  }
  /* Generate a new random digits filled buffer and concatenate
     it with the rest of previous one */
  #generate() {
    const string = Math.random().toString().substring(2);
    this.#buffer = this.#buffer.substring(this.#index) + string;
    this.#index = 0;
  }
  /* Get a random value and prepare itself for a next get */
  get value() {
    const string = this.#buffer.substring(this.#index);
    this.#index += 2;

    /* Generate a new random buffer if a previous one is almost empty */
    if ((this.#buffer.length - this.#index) < 2)
      this.#generate();

    return Number('.' + string);
  }
  /* Return a weighted random fraction as a coefficient */
  fraction(weight) {
    return (this.value * weight);
  }
  /* Return a coefficient for the whole, without a weighted random fraction */
  fractionLess(weight) {
    return (1 - this.value * weight);
  }
}
