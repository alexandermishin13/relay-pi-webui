await dict.load('/languages/lang_');

export class Scenery {
  static #saveUrl = '/plugins/aqua-z/aqua-z_save.php';
  static paramsList = ['left', 'top', 'width', 'height', 'scale'];
  static instances = {};
  static cookies = getCookies();
  constructor(id, element = null) {
    this.id = id;
    this.decorations = {};
    this.creatures = {};
    if (Object.is(element, null)) {
      this.element = document.getElementById(id);
    } else {
      this.element = element;
    }
  }
  /*
   * Load edit page for the scenery handler
   */
  loadEditor = async () => {
    window.location.href = '/plugins/aqua-z/aqua-z_edit.php?scenery=' + this.id;
  }
  /*
   * Save the decorations config
   */
  async save() {
    let sceneryToSave = {};
    let decosToSave = {};
    for (const [name, instance] of Object.entries(this.decorations)) {
      let paramsToSave = {};
      for (const param of this.constructor.paramsList) {
        paramsToSave[param] = instance[param];
      }
      decosToSave[name] = paramsToSave;
    }

    sceneryToSave[this.id] = decosToSave;
    const saveRequest = new Request(
      this.constructor.#saveUrl, {
        method: 'POST', cache: 'no-cache', headers: new Headers(), body: JSON.stringify(sceneryToSave)
      }
    );

    const data = await fetchJson(saveRequest);

    if (data.message !== undefined) alert(_(data.message));
    if (data.error === 'OK') redirectToMain();
  }
  /*
   * Create Save and Cancel buttons
   */
  createSaveCancelButtons(container) {
    const p = document.createElement('p');
    const buttonSave = document.createElement('button');
    const buttonAway = document.createElement('button');
    buttonSave.innerText = _('Save');
    if (authenticated) {
      buttonSave.addEventListener('click', (e) => this.save());
    } else {
      buttonSave.disabled = true;
    }
    buttonAway.innerText = _('Cancel');
    buttonAway.addEventListener('click', redirectToMain);
    p.classList.add('buttons');
    p.append(buttonSave, ' ', buttonAway);
    container.appendChild(p);
  }
  /*
   * Create a label for the scenery
   */
  createDecoLabels() {
    for (const [decoId, deco] of Object.entries(this.decorations)) {
      deco.createLabel();
    }
  }
  /*
   * Create a configuration anchor for the scenery
   */
  createConfigureAnchor() {
    const anchor = document.createElement('span');
    anchor.addEventListener(
      'click', this.loadEditor, passiveSupported ? { passive: true } : false
    );
    anchor.classList.add('anchor');
    this.element.classList.add('configurable');
    this.element.appendChild(anchor);
  }
  /*
   * Create configuration anchors for all sceneries
   */
  static createConfigureAnchors() {
    for (const [tankId, tankInstance] of Object.entries(Scenery.instances)) {
      tankInstance.createConfigureAnchor();
    }
  }
  /*
   * Control animation by checkbox. Save status in cookies
   */
  static animationControl(e) {
    if (e.type !== 'change') return;

    const elem = e.target;
    if (elem.checked) {
      for (const scenery of Object.values(Scenery.instances)) {
        for(const creature of Object.values(scenery.creatures)) {
          creature.animation.pause();
        }
      }
      document.documentElement.style.setProperty('--animps', 'paused');
    } else {
      for (const scenery of Object.values(Scenery.instances)) {
        for(const creature of Object.values(scenery.creatures)) {
          creature.animation.play();
        }
      }
      document.documentElement.style.setProperty('--animps', 'running');
    }
    setCookie("paused", elem.checked, true);
  }
  /*
   * Initialize an existing checkbox for pausing animations
   */
  static setAnimationPauseCheckbox(id) {
    const elem = document.getElementById(id);
    const descr = _('Pause');
    elem.labels[0].setAttribute('aria-label', descr);
    if ((elem === null) || (elem.type !== 'checkbox')) return;

    elem.checked = (Scenery.cookies.paused === 'true');
    Scenery.animationControl({ type: 'change', target: elem });
    elem.addEventListener('change', Scenery.animationControl );
  }
}
