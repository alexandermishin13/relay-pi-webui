<?php
include_once $config['document_root'].'/config/config_aqua-z.inc.php';
include_once $config['document_root'].'/plugins/aqua-z/aqua-z_functions.inc.php';
$plural = count($aqua_z_images) > 1;

function tankEvents($events) {
    global $js_switches;
    global $image_blank, $dbJson;

    foreach ($events as $name => $switch) {
        $js_switches[$name] = $switch['image'];
        /* Collect <img>-tag params if they exists 
         * for to combine them in the tag
         */
        $params = objectParams($switch);
        echo "<img id=\"image_{$name}\" alt=\"\" data-relay-name=\"{$name}\" $params", "src=\"{$image_blank}\">", PHP_EOL;
    }
};

/**
 * Draw the whole aquarium
 */
function compileAquarium($tank, $components) {
    echo "<div id=\"${tank}\" class=\"stack ${tank}\">", PHP_EOL;
    /* Decorations, fish and changeable parts */
    tankDecor($components['decorations']);
    fishSchools($components['fish']);
    tankEvents($components['switches']);
    echo '</div>', PHP_EOL;
}

?>
<section id="section_aqua-z" class="collapsible">
<hr><h2 data-l10n>Fish Tank<?php echo ($plural)?'s':''; ?></h2>
<div class="plugin_aqua_z">
<?php
/**
 * There may be multiple complex objects.
 */
$js_switches = [];
$js_tanks = [];
if (isset($aqua_z_images) && is_array($aqua_z_images)) {
    foreach ($aqua_z_images as $tank => $components) {
        $js_tanks[$tank] = [];
        if (isset($components['fish']))
            $js_tanks[$tank]['fish'] = array_keys($components['fish']);
        if (isset($components['deco']))
            $js_tanks[$tank]['fish'] = array_keys(array_filter($components['decorations'], function($v, $k) { return $v['class'] == 'plants'; }, ARRAY_FILTER_USE_BOTH));

        compileAquarium($tank, $components);
    }
}
?>
</div>
<div>
<span class="animation-control">
<input id="toggle-animation" type="checkbox">
<label for="toggle-animation" class="pause-animation"></label>
</span>
</div>
</section>
<script>
const images = <?php echo json_encode($js_switches, JSON_NUMERIC_CHECK) ?>;
const tanks = <?php echo json_encode($js_tanks, JSON_NUMERIC_CHECK)?>;
</script>
<script type="module" src="/plugins/aqua-z/aqua-z.js" defer></script>
