import {Decoration as Plant} from './Decoration.js';
import {Creature as Fish} from './Creature.js';
import {Gourami, School} from './Creature.js';
import {Scenery as Tank} from './Scenery.js';
import Random from './Random.js';

await dict.load('/plugins/aqua-z/lang/');

const random = new Random();

/* Create plants and fish for tanks */
for (const [tankId, tankObjects] of Object.entries(tanks)) {
  const tankElement = document.getElementById(tankId);
  if (Object.is(tankElement, null)) continue;

  const tank = new Tank(tankId, tankElement);
  Tank.instances[tankId] = tank;
  /* For every decoration */
  let delay = 0;
  if ('deco' in tankObjects) {
    for (const decoId of tankObjects['deco']) {
      delay += 1000 + random.value * 1000; // start time shift
      const deco = new Plant(decoId, tank.element);
      deco.decoElement.style.animationDelay = delay + 'ms';
      tank.decorations[decoId] = deco;
    }
  }
  /* For every school of the tank */
  let i = 0;
  if ('fish' in tankObjects) {
    for(const type of tankObjects['fish']) {
      const areas = document.getElementsByClassName(type);
      /* for every fish of the type*/
      for(const specimenArea of areas) {
        const creature = new Fish.derived[type](specimenArea, tank, delay = 400);
        tank.creatures[creature.id] = creature;
      }
    }
  }
}

Tank.setAnimationPauseCheckbox('toggle-animation');

/* When a container for controls exist add them. Otherwise add configure anchors */
const controls = document.querySelectorAll('.controls')[0];
if ((controls !== undefined) && !Object.is(controls, null)) {
  const tankElement = document.getElementsByClassName('stack')[0];
  const tank = Tank.instances[tankElement.id];
  tank.createDecoLabels();
  Plant.createControls(controls);
  tank.createSaveCancelButtons(controls);
} else {
  Tank.createConfigureAnchors();

  /* Group switchboard entries by the tank name and sort */
  const order = Array.from(document.querySelectorAll('.stack [data-relay-name]')).reduce(
    (o, r) => ({ ...o, [r.dataset.relayName]: r.parentElement.id}), {}
  );
  const entries = Array.from(document.querySelectorAll('.switchboard [data-relay-name]')).sort(
    (a,b) => (order[a.dataset.relayName]+a.innerText).localeCompare(order[b.dataset.relayName]+b.innerText)
  );
  entries[0].parentElement.replaceChildren(...entries);
}

/* Localize the elements */
const l10ns = document.querySelectorAll('[data-l10n]');
for (const elem of l10ns) {
  const desc = elem.innerText;
  elem.innerText = _(desc);
}
