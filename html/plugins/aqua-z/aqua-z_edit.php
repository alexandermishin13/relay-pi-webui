<?php
$tank = $_GET['scenery'];

//if (!$tank) {
//    header('Location: /', true, 307);
//    exit(0);
//}

include_once $_SERVER['DOCUMENT_ROOT'] . '/config/config.inc.php';
include_once $config['document_root'].'/config/config_aqua-z.inc.php';
include_once $config['document_root'].'/plugins/aqua-z/aqua-z_functions.inc.php';

/* Probe for a language include with constants */
if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    $probelang = $config['document_root'] . '/languages/lang_' . $lang . '.inc.php';
    if (file_exists($probelang)) {
        include $probelang;
    }
} else {
    $lang = 'en';
}
include $config['document_root'] . '/languages/lang_en.inc.php';

session_start();
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE HTML>
<html lang="<?php echo $lang ?>">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="/styles/styles.css">
<link rel="stylesheet" href="/plugins/aqua-z/styles/styles.css">
<link rel="stylesheet" href="/plugins/aqua-z/styles/fish.css">
<link rel="stylesheet" href="/plugins/aqua-z/styles/tank_<?php echo $tank ?>.css">
<link rel="stylesheet" href="/plugins/aqua-z/styles/animation.css">
<script src="/js/Dictionary.js"></script>
<script src="/js/functions.js" async></script>
</head>
<body>
<h1 data-l10n><?php echo $config['title'] ?></h1>
<section>
<hr><h2><span data-l10n>Fish Tank</span><?php echo '::' . $tank ?></h2>
<div class="plugin_aqua_z">
<?php
$js_tanks = [];
if (isset($aqua_z_images[$tank]) && is_array($aqua_z_images[$tank])) {
    $components = $aqua_z_images[$tank];
    $js_tanks[$tank] = [
        'fish' => array_keys($components['fish']),
        'deco' => array_keys(array_filter($components['decorations'], function($v, $k) { return $v['class'] == 'plants'; }, ARRAY_FILTER_USE_BOTH))
    ]; 
    echo "<div id=\"${tank}\" class=\"stack ${tank}\">", PHP_EOL;
    /* Decorations and fish */
    tankDecor($components['decorations']);
    fishSchools($components['fish']);
    echo '</div>', PHP_EOL;
}
?>
<div class="animation-control">
<input id="toggle-animation" type="checkbox">
<label for="toggle-animation" class="pause-animation"></label>
</div>
</div>
</section>
<section>
<hr><h2 data-l10n>Labels</h2>
<div class="labels">
<p>
</p>
</div>
</section>
<section>
<hr><h2 data-l10n>Controls</h2>
<div class="controls">
</div>
</section>
<script>
const authenticated = <?php echo isset($_SESSION['username']) ? 'true' : 'false' ?>;
dict.load('/languages/user/lang_');
const tanks = <?php echo json_encode($js_tanks, JSON_NUMERIC_CHECK) ?>;
</script>
<script type="module" src="/plugins/aqua-z/aqua-z.js" defer></script>
<script src="/plugins/aqua-z/Label.js" async></script>
<script src="/plugins/aqua-z/Controls.js" async></script>
</body>
</html>
