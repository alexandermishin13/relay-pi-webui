<?php
/**
 * Get common html paremeters from a configured object
 */
function objectParams($object) {
    $params = [];
    foreach(['class'] as $param) {
        if (isset($object[$param])) {
            array_push($params, $param . '="' . $object[$param] . '"');
        }
    }
    $style = [];
    foreach(['scale', 'left', 'right', 'bottom', 'top', 'width', 'height', 'max-height'] as $param) {
        if (isset($object[$param])) {
            array_push($style, $param . ': ' . $object[$param] . ';');
        }
    }
    if ($style) {
      array_push($params, 'style="' . implode(' ', $style) . '" ');
    }

    return implode(' ', $params);
}

/**
 * Decorate a tank with images
 */
function tankDecor($decorations) {
    foreach ($decorations as $id => $object) {
        // Turn it to array if it is a only url
        $images = $object['image'];
        if (!is_array($images)) $images = array($images);

        // Get common html parameters and construct stack of images
        $params = objectParams($object);
        foreach ($images as $number => $url) {
            echo "<img id=\"${id}\" alt=\"${id}\" ${params} src=\"${url}\">", PHP_EOL;
        }
    }
}

/**
 * Populate the aquarium with schools of fish
 */
function fishSchools($fish) {
    foreach ($fish as $class => $specimens) {
        $dataSurface = ($specimens['surface']) ? 'data-surface="fixed" ': '';
        foreach ($specimens['image'] as $number => $url) {
            /* No width nor height of images here.
             * Their values differ from real ones and are set as css-parameters
             */
            $school_id = $class.$number;
            echo "<div id=\"${school_id}\" class=\"$class\"><img alt=\"${school_id}\" class=\"fish\" ${dataSurface}src=\"$url\"></div>", PHP_EOL;
        }
    }
}
?>
