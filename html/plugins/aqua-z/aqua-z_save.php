<?php

//namespace RelayPi\WebUI;

include_once $_SERVER['DOCUMENT_ROOT'] . '/config/config.inc.php';
include_once $config['document_root'].'/config/config_aqua-z.inc.php';

/* Probe for a language include with constants */
if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    $probelang = $config['document_root'] . '/languages/lang_' . $lang . '.inc.php';
    if (file_exists($probelang)) {
        include $probelang;
    }
} else {
    $lang = 'en';
}
include $config['document_root'] . '/languages/lang_en.inc.php';

session_start();
header('Content-type: application/json');

$result = ['error' => 'notAuthorized'];

if (isset($_SESSION['username'])) {
    /* Try to load a config json */
//    Sensors::readConfig($config['path_root'] . '/' . $config['db_json'], true);
//    $dbJson = & Sensors::$config;

    /* Get JSON with changes made */
    $parameters = [
        'left' => 'px',
        'top' => 'px',
        'width' => 'px',
        'height' => 'px',
        'scale' => ''
    ];
    $data = json_decode(file_get_contents('php://input'), true);
    $decoDbJson = $aqua_z_attributes;
    foreach ($data as $tank => $decorations) {
        if (!isset($aqua_z_images[$tank])) continue;
        $d = [ 'decorations' => [] ];
        foreach ($decorations as $id => $attributes) {
            if (!isset($aqua_z_images[$tank]['decorations'][$id])) continue;
            $a = [];
            foreach ($parameters as $parameter => $unit) {
                if (!isset($attributes[$parameter])) continue;
                $a[$parameter] = $attributes[$parameter] . $unit;
            }
            $d['decorations'][$id] = $a;
        }
        $decoDbJson[$tank] = array_merge($decoDbJson[$tank], $d);
    }

    $result['error'] = 'OK';
    $result['message'] = 'Decorations saved';

    $file = fopen($config['path_root'] . '/db/aqua-z.json','w');
    fwrite($file, json_encode($decoDbJson, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    fclose($file);
}

echo json_encode($result, JSON_UNESCAPED_SLASHES);

?>
