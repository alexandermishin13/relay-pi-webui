<section id="section_aqua" class="collapsible">
<hr>
<h2 data-l10n>Aquariums</h2>
<div class="plugin_aqua">
<?php
include_once $config['document_root'].'/config/config_aqua.inc.php';

if (isset($images) && is_array($images)) {
  $js_switches = array();
  foreach ($images as $class => $parts) {
    $column = 0;
?>
<table id="<?php echo $class ?>" class="<?php echo $class ?>">
<caption><?php echo get_name($class) ?></caption>
<tr>
<?php
    foreach ($parts as $name => $part) {
      /* You must control columns by 'column' attribute of an element of $images array.
       * If You can't they will be just incremented
       */
      $column = (array_key_exists('column', $part)) ? $part['column'] : $column;
      $js_switches[$name] = $part['image'];
      $column++;
      /* Collect <img>-tag params if they exists 
       * for to combine them in the tag
       */
      $image_param = array('src="'.$image_blank.'"', 'id="image_'.$name.'"');
      if (array_key_exists('width', $part)) {
        array_push($image_param, 'width='.$part['width']);
      };
      if (array_key_exists('height', $part)) {
        array_push($image_param, 'height='.$part['height']);
      };
      /* If exists an outlet name in json with the same id
       * as this part of object image use it as a tip for image.
       */
      $outlets = $dbJson['outlets'];
      if (array_key_exists($name, $outlets)) {
        $outlet = $outlets[$name];
        /* If 'name' explicitly defined as parameter of json node
         * this one is used, otherwise will tried a constant like a NAME_DESC
         * and then  name itself
         */
        $desc = array_key_exists('name', $outlet) ? $outlet['name'] : get_name($name);
        array_push($image_param, 'title="'.$desc.'"');
      };
?>
<td><img <?php echo implode(' ', $image_param) ?>></td>
<?php
    };
?>
</tr>
</table>
<?php
  };
};
?>
</div>
</section>
<script>
dict.load('/plugins/aqua/lang/');
const images = <?php echo json_encode($js_switches, JSON_NUMERIC_CHECK) ?>;
</script>
