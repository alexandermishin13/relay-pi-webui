import PeriodicRequest from '/js/PeriodicRequest.js';

/* Puts the ntp status into the plugin element */
async function displayNtpStatus(data) {
  let classes = ['good', 'good'];
  const ntpdata = [data['stratum'], data['reference']];
  const message = data['message'];

  if (typeof(ntpdata[0]) === 'number') {
    if (ntpdata[0] === 3) {
      classes[0] = 'warn';
    } else if (ntpdata[0] > 3) {
      classes[0] = 'alarm';
    }

    if (ntpdata[1] === 'public pool') {
      classes[1] = 'warn';
    } else if (ntpdata[1] === 'unknown') {
      classes[1] = 'alarm';
    }

    for (let i = 0; i < ntpdata.length; i++) {
      const el = ntp.children[i*2+1];
      el.innerText = ntpdata[i];
      el.className = classes[i];
      if (message) el.title = message;
    }
  }
}

await dict.load('/plugins/ntp-status/lang/');

/* Creates the plugin element onto the page */
const ntp = document.createElement('div');
ntp.id='ntp';
ntp.className='auth';
const ntpStratum = document.createElement('span');
ntpStratum.innerText = _('NTP Stratum');
const ntpRefId = document.createElement('span');
ntpRefId.innerText = _('Reference ID');
ntp.append(ntpStratum, ':', document.createElement('span'), ', ', ntpRefId, ':', document.createElement('span'), ' |');

/* Shows default (not good) ntp status */
displayNtpStatus({ stratum: 10, reference: 'local clock' });
document.getElementsByClassName('auth')[0].insertAdjacentElement('afterend', ntp);

/* Periodically renews an ntp status when is not in demo mode */
if (!demoMode) {
  const requestNtp = PeriodicRequest.registerRequestType('ntp', '/requests/ntp_status.php', 60000);
  requestNtp.addHandler(displayNtpStatus);
  requestNtp.registerPlatform('ntp');
}
