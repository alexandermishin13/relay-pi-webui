<?php

namespace RelayPi\WebUI;

function htmlDescription($descriptions) {
    return join(' ', array_map(function($value) {
        return '<span data-l10n>'.$value.'</span>';
    }, $descriptions));
}

?>
<section id="section_sensors" class="collapsible">
<hr><h2 data-l10n>Sensors</h2>
<div class="plugin_sensors">
<ul class="platforms">
<?php
foreach($dbJson['platforms'] as $platform_name => $platform) {
?>
<li class="collapsible"><?php echo $platform_name, PHP_EOL ?>
<ul id="<?php echo $platform_name ?>_details" class="details">
<?php
  $platformObject = Sensors::getPlatformObject($platform_name);
?>
<li><span data-l10n>Type</span>: <?php echo $platformObject->platformType ?></li>
<?php
if (isset($_SESSION['username'])) {
  foreach ($platformObject->getInfo() as $infoName => $infoValue) {
    if (!empty($infoValue)) {
?>
<li><span data-l10n><?php echo $infoName ?></span>: <?php echo $infoValue ?></li>
<?php
    }
  }
?>
<li><span data-l10n>Host</span>: <?php echo $platformObject->platformHost ?></li>
<?php
}
?>
<li><span data-l10n>Sensors</span>:
<ul class="sensors">
<?php
    foreach (Sensors::iterateRelaysPlatform($platform_name) as $relayName => $relayObject) {
        $sensorsClass = ($relayObject->active)?'outlet':'outlet disabled';
?>
<li class="<?php echo $sensorsClass ?>" data-name="<?php echo $relayName ?>"><?php echo $relayName ?>
<ul>
<?php
        if ($relayName !== $relayObject->description) {
            $description = htmlDescription($relayObject->descriptions);
?>
<li><span data-l10n>Description</span>: <?php echo $description ?></li>
<?php
        }
?>
<li><span data-l10n>Value</span>: <span data-value data-l10n>N/A</span></li>
</ul>
</li>
<?php
    }
    foreach (Sensors::iterateThermPlatform($platform_name) as $thermName => $thermObject) {
?>
<li class="thermometer" data-name="<?php echo $thermName ?>"><?php echo $thermName ?>
<ul>
<?php
        if ($thermName !== $thermObject->description) {
            $description = htmlDescription($thermObject->descriptions);
?>
<li><span data-l10n>Description</span>: <?php echo $description ?></li>
<?php
        }
?>
<li><span data-l10n>Value</span>: <span data-value data-l10n>N/A</span></li>
</ul>
</li>
<?php
    }
?>
</ul>
</li>
</ul>
</li>
<?php
}
?>
</ul>
</div>
</section>
<script type="module" src="/plugins/sensors/sensors.js" defer></script>
