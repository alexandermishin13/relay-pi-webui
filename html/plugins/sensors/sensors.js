import PeriodicRequest from '/js/PeriodicRequest.js';

await dict.load('/plugins/sensors/lang/');

/* Get from data and display values from outlets */
async function displaySwitchStatus(data) {
  for(const [switchName, switchStatus] of Object.entries(data)) {
    /* Skip the record if data is not correct */
    if (switchStatus === undefined) continue;

    const elem = document.querySelectorAll('.outlet[data-name="' + switchName + '"] [data-value]');
    if (elem.length !== 1) continue;

    elem[0].innerText = _(switchStatus);
  }
}

/* Get from data and display values from thermometers */
async function displayThermStatus(data) {
  for (const [thermName, thermometer] of Object.entries(data)) {
    const containerId = thermName;
    if (!containerId) continue;

    const elem = document.querySelectorAll('.thermometer[data-name="' + containerId + '"] [data-value]');
    if (elem.length !== 1) continue;

    elem[0].innerText = thermometer.temperature;
  }
}

/* Add periodic task handlers to display sensor module values */
function addSensorHandlers() {
  for (const [name, platformPeriodicTask] of Object.entries(PeriodicRequest.instances)) {
    switch (name) {
      case 'outlets':
        platformPeriodicTask.addHandler(displaySwitchStatus);
        break;
      case 'thermometers':
        platformPeriodicTask.addHandler(displayThermStatus);
        break;
    }
  }
}

/* Add job to add additional periodic task handlers */
PeriodicRequest.finalJobs.push(addSensorHandlers);

/* Handle all collapsible sections */
const collapsibleItems = document.querySelectorAll('li.collapsible');
for (const element of collapsibleItems) {
  collapsible(element);
}

/* Make the container collapsible */
function collapsible(anchor) {
  const cookies = getCookies();
  /* Collapse the section if the cookies say that */
  const content = anchor.children[0];
  if (cookies[content.id] == 'false') {
    setContentVisible({ target: anchor });
  }
  anchor.addEventListener('click', setContentVisible);

  /* Anchor click event handler */
  function setContentVisible(event) {
    const anchor = event.target;
    const content = anchor.children[0];
    const action = (content.style.display == 'none');

    setCookie(content.id, action, true);
    if (action) {
//      element.classList.remove('collapsed');
      content.style.display = null;
    } else {
//      element.classList.add('collapsed');
      content.style.display = 'none';
    }

    if(this === event.target)
      event.stopPropagation();
  }
}

