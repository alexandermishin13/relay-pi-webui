<script>
<?php
include_once $config['document_root'].'/config/config_temperature.inc.php';

/* Generate fake thermometers values for demo mode */
if ($config['demo_mode']) {

    $js_therms = [];
    foreach ($thermometers as $thermName => $thermometer) {
        $js_therms[$thermName] = ['id' => $thermometer['id'], 'temperature' => 26.5];
    }
?>
const thermDummies=<?php echo json_encode($js_therms) ?>;
<?php
} else {
    $t_platforms = [];
    foreach ($thermometers as $thermName => $thermConfig) {
        $thermometer = $dbJson['thermometers'][$thermName];
        $platform = platform_name($thermometer['platform']);
        $t_platforms[$platform] = $t_platforms[$platform] ?? true;
    }
?>
const thermPlatforms = <?php echo json_encode(array_keys($t_platforms)) ?>;
<?php
}
?>
</script>
<script type="module" src="/plugins/temperature/temperature.js" defer></script>
