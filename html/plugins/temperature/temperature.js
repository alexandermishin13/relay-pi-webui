import PeriodicRequest from '/js/PeriodicRequest.js';

async function displayTemperature(data) {
  for (const [thermName, thermometer] of Object.entries(data)) {
    const containerId = thermometer.id;
    if (!containerId) continue;

    const elem = document.getElementById(containerId);
    if (!elem) continue;

    var span = document.getElementById(thermName);
    /* Add a new span to display a value if not any */
    if (!span) {
      span = document.createElement('span');
      span.id = thermName;
      span.className = 'temperature';
      /* 'elem' can be a table or a simple element */
      if(elem.tagName.toLowerCase() === 'table'){
        /* Last cell in first row */
        const cells = elem.rows[0].cells;
        const col = cells.length - 1;
        cells[col].appendChild(span);
      } else {
        elem.appendChild(span);
      }
    }
    span.innerHTML = thermometer.temperature;
  }
}

/* Periodically renews a list of temperature sensors when not is in demo mode */
if (demoMode) {
  const demoTemperature = (platform) => {
    return thermDummies;
  }
  const emulateThermometers = PeriodicRequest.registerTaskType('thermometers', demoTemperature, 60000);
  emulateThermometers.addHandler(displayTemperature);
  emulateThermometers.registerPlatform(name, {n: name});
} else {
  const requestThermometers = PeriodicRequest.registerRequestType('thermometers', '/requests/temperature.php', 60000);
  requestThermometers.addHandler(displayTemperature);
  /* Run requests to all platforms */
  for (const name of thermPlatforms) {
    requestThermometers.registerPlatform(name, {n: name});
  }
}
