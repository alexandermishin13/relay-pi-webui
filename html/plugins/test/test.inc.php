<section id="section_test" class="collapsible">
<hr><h3 data-l10n>Test</h3>
<div class="plugin_test">
<div class="stack aqua1">
<img class="lighting" src="/plugins/test/images/mainLighting.svg">
<img class="airpump" src="/plugins/test/images/airpump.svg">
<img class="ground" src="/plugins/test/images/ground.png">
<svg width="380px" height="270px">
<g stroke-width="0">
<rect opacity=".05" fill="#309000" x="10" y="60" width="360" height="200" />
<rect opacity=".05" fill="#0000ff" x="10" y="90" width="360" height="170" />
<path opacity=".1" stroke-width="7" fill="none" stroke="#309000" d="M10,60v200h360v-200" />
</g>
</svg>
<img class="reflection" src="/plugins/test/images/reflection.svg">
</div>
<div class="stack aqua2">
<img class="lighting" src="/plugins/test/images/sideLighting.svg">
<img class="ground" src="/plugins/test/images/ground2.png">
<svg width="260px" height="270px">
<g stroke-width="0">
<rect opacity=".05" fill="#309000" x="10" y="60" width="240" height="200" />
<rect opacity=".05" fill="#0000ff" x="10" y="90" width="240" height="170" />
<path opacity=".1" stroke-width="7" fill="none" stroke="#309000" d="M10,60v200h240v-200" />
</g>
</svg>
<img class="reflection" src="/plugins/test/images/reflection.svg">
</div>
</div>
</section>
<script type="module">
await dict.load('/plugins/test/lang/');
</script>
