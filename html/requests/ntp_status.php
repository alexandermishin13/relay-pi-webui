<?php

/* Define Your home time sources as its names or just as 'private'.
 * All other ones will be displayed as 'public'.
 */
$host_name = array(
    '192.168.1.1' => 'router.home',
);

/* Check if php-sockets is installed.
 * Required for ntp connection
 */
if (!extension_loaded('sockets')) {
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

    /* Probe for a language include with constants.
     * Still include defines later on, if some constants were missing
     */
    $probelang = __DIR__ . '/languages/lang_' . $lang . '.inc.php';
    if (file_exists($probelang)) {
        include $probelang;
    }
    include __DIR__ . '/languages/lang_en.inc.php';

    $json = array(
        'stratum' => 'unknown',
        'reference' => 'unknown',
        'message' => sprintf(ERROR_EXTENSION_LOADED, 'sockets')
    );

    header('Content-type: application/json');
    echo json_encode($json, JSON_NUMERIC_CHECK);
    exit(0);
}

// No changes needed below
function ntp_time($host) {
    /* Create a socket and connect to NTP server */
    $socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    socket_connect($socket, $host, 123);

    /* Send request */
    $request = "\010" . str_repeat("\0", 47);
    socket_send($socket, $request, strlen($request), 0);

    /* Receive response and close socket */
    if (socket_recv($socket, $response, 48, MSG_WAITALL) === false) {
        $data['error'] = socket_last_error($socket);
    } else {
        $data = unpack('C1li_vn_mode/C1stratum/C1poll/C1precision/N1delay/N1dispersion/A4reference/N8', $response);
    }
    socket_close($socket);

    return $data;
}

session_start();

/* IP-address of ntp server for control its stratum and reference.
   I don't see why You may need another IP-address... but maybe...
 */
$timeserver = "127.0.0.1";
$timercvd = ntp_time($timeserver);

$json = array();
if (isset($timercvd['error'])) {
    $json['error'] = $timercvd['error'];
} else {
    $li_vn_mode = $timercvd['li_vn_mode'];
    $mode = $li_vn_mode % 8;
    $li = $li_vn_mode >> 6; // Shift 'mode' and ignore 'vn'

    /**
     * NTP is number of seconds since 0000 UT on 1 January 1900
     * Unix time is seconds since 0000 UT on 1 January 1970
     * $timestamp = sprintf('%u', $timercvd[1]) - 2208988800;
     */
    $json['stratum'] = $timercvd['stratum'];

    /* For stratum 0-1 it is a string, for 2-15 is a packed ip-address */
    if ($json['stratum'] < 2) {
        $json['reference'] = $timercvd['reference'];
    } else {
        $ref_ip = long2ip(unpack('N', $timercvd['reference'])[1]);
        $json['reference'] = $host_name[$ref_ip] ?? 'public pool';
    }
}

header('Content-type: application/json');
echo json_encode($json, JSON_NUMERIC_CHECK);
?>
