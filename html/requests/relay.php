<?php

namespace RelayPi\WebUI;

include_once $_SERVER['DOCUMENT_ROOT'].'/config/config.inc.php';

header('Content-type: application/json');
session_start();

/* Initialize the static object */
Sensors::readConfig($config['path_root'].'/'.$config['db_json'], true);

/* Get status and switch relays
 * Prepare an array of relay states for visualization
 * States are strings 'ON' or 'OFF'
 */
$visual = [];

$request = json_decode(file_get_contents('php://input'), true);
$job = $request['t'];
switch ($job) {
    /* Get status of relays for a named platform */
    case 'status':
        $platformName = $request['n'];
        foreach (Sensors::iterateRelaysPlatformState($platformName, true) as $relayName => $relayState) {
            if (isset($relayState)) {
                $visual[$relayName] = $relayState;
            }
        }
        /* if result is empty turn off all the platform switches */
        if (!$visual) {
            foreach (Sensors::iterateRelaysPlatform($platformName) as $relayName => $relayConfig) {
                $visual[$relayName] = 'off';
            }
        }
        break;
    /* Switch a relay to the state */
    case 'on';
    case 'off';
    case 'toggle':
        /* Only if authenticated */
        if (isset($_SESSION['username'])) {
            $relayName = $request['n'];
            $relayState = Sensors::relaySwitch($relayName, $job, true);
            if (isset($relayState)) {
                $visual[$relayName] = $relayState;
            }
        }
        break;
}

echo json_encode($visual, JSON_NUMERIC_CHECK);
?>
