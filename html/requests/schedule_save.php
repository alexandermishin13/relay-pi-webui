<?php

namespace RelayPi\WebUI;

include_once $_SERVER['DOCUMENT_ROOT'] . '/config/config.inc.php';

session_start();
header('Content-type: application/json');

$result = [];

if (isset($_SESSION['username'])) {
    /* Try to load a config json */
    Sensors::readConfig($config['path_root'] . '/' . $config['db_json'], true);
    $dbJson = & Sensors::$config;

    /* Get JSON with changes made */
    $relays = json_decode(file_get_contents('php://input'), true);
    /* For each outlet changed (one by the fact) */
    regenerateTimers($relays, $dbJson);
    /* Save changes and remake crontab */
    $saved = saveConfigJson($config['path_root'] . '/' . $config['db_json'], $dbJson);
    makeCrontab($dbJson['outlets']);

    $result['error'] = 'OK';
} else {
    $result['error'] = 'notAuthorized';
}

/* Return a json-formatted result */
echo json_encode($result, JSON_NUMERIC_CHECK);


/**
 * Generate new 'timers' for changed outlets schedules, make
 * this changes to the json config for save it to the "relay.db" later
 */
function regenerateTimers($relays, &$dbJson) {
    /* Loop for each relay ... */
    foreach ($relays as $relayName => $relayData) {
        $timersJson = [];
        $timers = $relayData['timers'];

        /* ...  all its timers */
        foreach ($timers as $timer) {
            $timerJson = [
                'time' => $timer[0],
                'action' => $timer[1]
            ];

            /* Check array of weekdays. Omit if all seven days or no one at all */
            if (is_array($timer[2])) {
                $dows = array_unique($timer[2], SORT_NUMERIC);
                $dowsCount = count($dows);
                if ($dowsCount > 0 && $dowsCount <= 7) {
                    if ($dows != range(1,7)) {
                        $timerJson['weekday'] = $dows;
                    }
                }
            }

            /* Timer to save */
            array_push($timersJson, $timerJson);
        }

        /* Made changes to the original JSON config */
        $dbJson['outlets'][$relayName]['timers'] = $timersJson;
    }
}

/**
 * Save the json config to the "relay.db"
 */
function saveConfigJson($configJson, $dbJson) {
    /* Encode nested arrays to JSON and prettyfy it some */
    $jsonStr = json_encode($dbJson, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    $jsonStr = preg_replace_callback(
        "~\"weekday\": \[([^\]]*)\]~",
        function($s) { return preg_replace('/\n[\s]+/', '', "\"weekday\": [$s[1]]"); },
        $jsonStr
    );

    /* Save changes and remake crontab */
    return file_put_contents($configJson, $jsonStr);
}

/**
 * Generate a Crontab collection and install it as a crontab file for a user
 *
 * @param array $outlets An 'outlets' section of a dict of config file
 */
function makeCrontab($outlets) {
    global $config;

    $crontab = new Crontab();
    /* Set the all relay to their actual states on boot */
    $job = new Crontab\Job($config['path_root'] . '/' . $config['cmd_init']);
    $job->setTime('@reboot');
    $crontab->addRemark('Run once on cron daemon is started');
    $crontab->addJob($job);
    foreach ($outlets as $name => $outlet) {
        /* Skip if 'active' exactly set to false */
        if ($outlet['active'] === false) {
            continue;
        }

        /* Add all jobs related to the outlet */
        $crontab->addRemark($name);
        $command = $config['path_root'] . '/' .$config['cmd_switch'];
        foreach ($outlet['timers'] as $timer) {
            $weekday = $timer['weekday'] ?? null;
            $parameter = escapeshellarg($name . '=' . $timer['action']);

            $job = new Crontab\Job($command, $parameter);
            $job->setTime($timer['time']);
            $job->setDOW($weekday);
            $crontab->addJob($job);
        }
    }

    /* Register the crontab in the system */
    $crontab->save();
}
?>
