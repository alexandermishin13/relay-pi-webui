<?php

namespace RelayPi\WebUI;

include_once $_SERVER['DOCUMENT_ROOT'].'/config/config.inc.php';
include_once $config['document_root'].'/config/config_temperature.inc.php';

$request = json_decode(file_get_contents('php://input'), true);
$platformName = $request['n'];

header('Content-type: application/json');

/* Initialize the static object */
Sensors::readConfig($config['path_root'].'/'.$config['db_json'], true);

$json = [];
$thermIterator = Sensors::iterateThermPlatformState($platformName, true);
foreach ($thermIterator as $thermName => $thermState) {
    if (!isset($thermometers[$thermName])) {
        continue;
    }

    $params = & $thermometers[$thermName];
    $containerId = $params['id'] ?? '';

    /* Round the result */
    $decimals = $params['decimals'] ?? 1;
    $temperature = number_format($thermState, $decimals);

    /* Set the result as a named thermometers value */
    $json[$thermName] = ['id' => $containerId, 'temperature' => $temperature];
}
/* if result is empty turn off the temperature values for all the platform thermometers */
if (!$json) {
    foreach (Sensors::iterateThermPlatform($platformName) as $thermName => $thermConfig) {
        $params = & $thermometers[$thermName];
        $containerId = $params['id'] ?? '';
        $json[$thermName] = ['id' => $containerId, 'temperature' => '~'];
    }
}

echo json_encode($json, JSON_NUMERIC_CHECK);
?>
