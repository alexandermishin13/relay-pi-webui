const ra = 12;
const center = ra + 2;
const h = 2.5;
const r3 = 6.5;
const num = 6;
const segment = Math.PI / num;
const xmlns = 'http://www.w3.org/2000/svg';
const svg = document.createElementNS(xmlns, 'svg');
const path = document.createElementNS(xmlns, 'path');
/* The edge of the first gear tooth begins half an segment before the zero point */
let angle = -segment / 2 + .05;
let delta = 0;
let r = ra - delta;
const x = (center + Math.cos(angle) * r).toPrecision(3);
const y = (center + Math.sin(angle) * r).toPrecision(3);
let p = `M${x},${y}`;
/* A couple of loops for a- and f-levels of each tooth of the gear */
for (let n = 1; n <= (2 * num); n++) {
  angle += segment;
  delta = Math.abs(delta - h);
  const x1 = (center + Math.cos(angle-.1) * r).toPrecision(3);
  const y1 = (center + Math.sin(angle-.1) * r).toPrecision(3);
  r = ra - delta;
  const x2 = (center + Math.cos(angle) * r).toPrecision(3);
  const y2 = (center + Math.sin(angle) * r).toPrecision(3);
  p += `A${r},${r} 0,0,1 ${x1},${y1}L${x2},${y2}`;
}
max = center + r3;
min = center - r3;
/* A filled with color gear with a transpared circle in its center */
p += `z M${max},${center}A${r3},${r3} 0,0,0 ${center},${min} ${r3},${r3} 0,0,0 ${min},${center} ${r3},${r3} 0,0,0 ${center},${max} ${r3},${r3} 0,0,0 ${max},${center}z`;
/* Draw the gear */
svg.setAttributeNS(null, 'viewPort', '0 0 28 28');
svg.setAttributeNS(null, 'fill', 'black');
svg.setAttributeNS(null, 'stroke', 'black');
svg.setAttributeNS(null, 'stroke-width', '0');
path.setAttributeNS(null, 'd', p);
svg.append(path);
document.body.appendChild(svg);
